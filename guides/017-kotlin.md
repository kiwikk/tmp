[<- Contents](README.md)

# Kotlin

## Contents
1. [Code style](#code-style)
    1. [Coding conventions](#coding-conventions)
    1. [ktlint](#ktlint)
    1. [detekt](#detekt)
    1. [Intellij plugins](#intellij-plugins)
1. [Data classes](#data-classes)
    1. [JSON](#json)
    1. [Cursor](#cursor)
    1. [State](#state)
    1. [Cells](#cells)
1. [Adapter](#adapter)
1. [Flux](#flux)
    1. [Actions](#actions)
    1. [Reducer](#reducer)
    1. [ViewModel](#viewmodel)
1. [Database queries](#database-queries)
1. [Useful extensions](#useful-extensions)

## Code style

### Coding conventions
We use an official Kotlin [style guide](https://kotlinlang.org/docs/reference/coding-conventions.html), simple as that.

### ktlint
On macOS you can install `ktlint` from Homebrew:
```
brew install ktlint
```

Check [here](https://ktlint.github.io/#getting-started) for other platforms.

Run it from the project folder like that:
```
ktlint --reporter='plain?group_by_file'
```

### detekt
On macOS you can install `detekt` from Homebrew
```
brew install detekt
```

Check [here](https://detekt.github.io/detekt/cli.html) for other platforms.

Run it from the project folder like that:
```
detekt --config detekt.yml
```

### Intellij plugins
detekt Intellij plugin is [available](https://plugins.jetbrains.com/plugin/10761-detekt). It should pick up the config from the project root by itself, but in case of issues:
1. Open Android Studio / Intellij IDEA settings
1. Go to Tools > detekt
1. Check the `Enable Detekt` and `Enable Formatting rules` checkboxes and uncheck all the others.
1. Select the `detekt.yml` in the project root as the `Configuration File`

## Data classes

### JSON
To parse a data class from JSON or to serialize it into JSON use the following pattern:
```kotlin
data class NewAircraft(
    val code: String?,
    val name: String?,
    val model: String?,
    val group: String?,
    val imageUrl: String?
) {

    constructor(json: AitaJson) : this(
        code = json.optString("code"),
        name = json.optString("name"),
        model = json.optString("model"),
        group = json.optString("group"),
        imageUrl = json.optString("image")
    )

    fun toJson(): AitaJson = AitaJson().apply {
        put("code", code)
        put("name", name)
        put("model", model)
        put("group", group)
        put("image", imageUrl)
    }
}
```

### Cursor
To parse a data class from `Cursor` or to serialize it into `ContentValues` use the following pattern:
```kotlin
data class NewAircraft(
    val code: String?,
    val name: String?,
    val model: String?,
    val group: String?,
    val imageUrl: String?
) {

    constructor(cursor: Cursor, holder: CursorColumnIndexHolder) : this(
        code = cursor.optString(holder.codeColIndex),
        name = cursor.optString(holder.nameColIndex),
        model = cursor.optString(holder.modelColIndex),
        group = cursor.optString(holder.groupColIndex),
        imageUrl = cursor.optString(holder.imageUrlColIndex)
    )

    fun toContentValues(): ContentValues = ContentValues().apply {
        put(AircraftTable.Column.CODE, code)
        put(AircraftTable.Column.NAME, name)
        put(AircraftTable.Column.MODEL, model)
        put(AircraftTable.Column.URL, imageUrl)
        put(AircraftTable.Column.GROUP_KEY, group)
    }

    class CursorColumnIndexHolder(cursor: Cursor) {
        val codeColIndex: Int = cursor.getColumnIndex(AircraftTable.Column.CODE)
        val nameColIndex: Int = cursor.getColumnIndex(AircraftTable.Column.NAME)
        val modelColIndex: Int = cursor.getColumnIndex(AircraftTable.Column.MODEL)
        val groupColIndex: Int = cursor.getColumnIndex(AircraftTable.Column.GROUP_KEY)
        val imageUrlColIndex: Int = cursor.getColumnIndex(AircraftTable.Column.URL)
    }
}
```

Note that we're using `Cursor.opt*` methods. It's an extension that also checks the column index and returns a default value if that index equals `-1`:
```kotlin
fun Cursor.optString(columnIndex: Int, fallback: String = ""): String =
    if (columnIndex == -1) {
        fallback
    } else {
        getStringOrNull(columnIndex) ?: fallback
    }
```

### State
Sometimes we have multiple classes for our state: state itself, view state, and some others. If the whole thing is not too long, you can place it all in a single file:
```kotlin
// File: TripDetailsState.kt

data class TripDetailsState(
    val trip: NewTrip?,
    val currentYear: Int,
    val toolbarState: TripDetailsToolbarState,
    val viewState: TripDetailsViewState,
    val oneTimeNavEvent: Event<TripDetailsOneTimeNav>?,
    val scrollListToTopEvent: Event<Boolean>?,
    val flightReviewsCheckedStateMap: Map<String, FlightReviewsState?>
) {
    companion object {
        fun newEmpty(currentYear: Int): TripDetailsState = TripDetailsState(
            null,
            currentYear,
            TripDetailsToolbarState.EMPTY,
            TripDetailsViewState.EMPTY,
            null,
            null,
            emptyMap()
        )
    }
}

data class TripDetailsViewState(
    val cells: List<MyTripsCell>,
    val scrollListToTopEvent: Event<Boolean>?
) {
    companion object {
        val EMPTY: TripDetailsViewState = TripDetailsViewState(emptyList(), null)
    }
}

data class TripDetailsToolbarState(
    val title: String,
    val subtitle: String,
    val statistics: TripStatistics,
    val isTabLayoutVisible: Boolean,
    val isPastTabSelected: Boolean
) {
    companion object {
        val EMPTY: TripDetailsToolbarState = TripDetailsToolbarState(
            "",
            "",
            TripStatistics.EMPTY,
            isTabLayoutVisible = false,
            isPastTabSelected = false
        )
    }
}

data class TripStatistics(
    val timeText: String?,
    val distanceText: String?,
    val flightsText: String?,
    val hotelsText: String?
) {
    val isEmpty: Boolean
        get() = hotelsText.isNullOrEmpty() && flightsText.isNullOrEmpty() && distanceText.isNullOrEmpty() && timeText.isNullOrEmpty()

    companion object {
        val EMPTY: TripStatistics = TripStatistics(null, null, null, null)
    }
}
```

### Cells
Sealed classes work great for cells:
```kotlin
sealed class MyTripsCell : Diffable<MyTripsCell> {

    data class Unsorted(val unsortedTripsCount: Int) : MyTripsCell() {
        override fun isSame(other: MyTripsCell): Boolean = other is Unsorted

        override fun isContentsSame(other: MyTripsCell): Boolean = this == other
    }

    data class Title(
        val unevenWeightTitle: UnevenWeightTitle?,
        val subtitle: String?,
        val timeText: String?
    ) : MyTripsCell() {

        override fun isSame(other: MyTripsCell): Boolean = this == other

        override fun isContentsSame(other: MyTripsCell): Boolean = this == other
    }

    data class ArriveAt(
        val arrivalText: String?,
        val stopDurationText: String?,
        val isDividerShown: Boolean,
        val terminalChangeText: String?
    ) : MyTripsCell() {

        override fun isSame(other: MyTripsCell): Boolean = this == other

        override fun isContentsSame(other: MyTripsCell): Boolean = this == other
    }

    data class AddSegment(val text: String) : MyTripsCell() {
        override fun isSame(other: MyTripsCell): Boolean = other is AddSegment

        override fun isContentsSame(other: MyTripsCell): Boolean = this == other
    }

    // ...
}
```

## Adapter
Sealed classes are great, but we still need to get some numbers from them to use as a view type. Use the following pattern for `RecyclerView.Adapter` with multiple view types:
```kotlin
class MyTripsAdapter(
    cells: List<MyTripsCell>,
    private val inflater: LayoutInflater,
    private val requestManager: RequestManager,
    // ...
) : AbsDiffableListAdapter<MyTripsCell, AbsMyTripsHolder>(cells) {

    private enum class ViewType {
        UNSORTED,
        TITLE,
        ARRIVE_AT,
        ADD_SEGMENT,
        // ...
    }

    private val MyTripsCell.viewType: ViewType
        get() = when (this) {
            is MyTripsCell.Unsorted -> ViewType.UNSORTED
            is MyTripsCell.Title -> ViewType.TITLE
            is MyTripsCell.ArriveAt -> ViewType.ARRIVE_AT
            is MyTripsCell.AddSegment -> ViewType.ADD_SEGMENT
            // ...
        }

    private val viewTypeValues = ViewType.values()

    override fun onCreateViewHolder(parent: ViewGroup, viewTypeOrdinal: Int): AbsMyTripsHolder = when (viewTypeValues[viewTypeOrdinal]) {
        ViewType.UNSORTED -> UnsortedMyTripsHolder(inflater, parent, requestManager, listener)
        ViewType.TITLE -> TitleMyTripsHolder(inflater, parent)
        ViewType.COVID_INFO -> CovidInfoMyTripsHolder(inflater, parent, requestManager, listener)
        ViewType.ARRIVE_AT -> ArriveAtMyTripsHolder(inflater, parent, requestManager)
        ViewType.ADD_SEGMENT -> AddSegmentMyTripsHolder(inflater, parent, requestManager, listener)
        // ...
    }

    override fun onBindViewHolder(holder: AbsMyTripsHolder, position: Int): Unit = holder.bind(getItem(position))

    override fun getItemViewType(position: Int): Int = getItem(position).viewType.ordinal

    interface Listener { ... }
}
```

## Flux

### Actions
Sealed classes are great for Flux Actions. Use the following pattern:
```kotlin
sealed class TripDetailsAction : Action {

    object ClickAddSegment : TripDetailsAction()
    object ClickClose : TripDetailsAction()
    object ClickEditTrip : TripDetailsAction()

    object ShowRestrictionsDialog : TripDetailsAction()

    data class TabSelected(val tabPosition: Int, val currentUtcSeconds: Long) : TripDetailsAction()

    data class TimeTick(val currentUtcSeconds: Long) : TripDetailsAction()

    data class Init(
        val trip: NewTrip,
        val flightReviewsCheckedStateMap: Map<String, FlightReviewsState?>,
        val currentUtcSeconds: Long
    ) : TripDetailsAction()

    data class Refresh(
        val tripIdToTripMapping: Map<String, NewTrip>,
        val currentUtcSeconds: Long
    ) : TripDetailsAction()

    data class UpdateTripFlightReviewsChecked(
        val key: String,
        val flightReviewsState: FlightReviewsState?,
        val currentUtcSeconds: Long
    ) : TripDetailsAction()
}
```

### Reducer
And your Reducer now looks like this:
```kotlin
class UnsortedReducer : Reducer<UnsortedState> {

    override fun acceptsAction(action: Action): Boolean = action is UnsortedAction

    override fun reduce(oldState: UnsortedState, action: Action): UnsortedState {
        return when (action) {
            is UnsortedAction.CloseHint -> oldState.copy(hintShown = false)
            is UnsortedAction.ShowError -> oldState.copy(errorEvent = Event(action.errorMessage))
            is UnsortedAction.Dismiss -> oldState.copy(dismissedEvent = Event(true))
            is UnsortedAction.MarkTrip ->
                // ...
            is UnsortedAction.DeleteTrip ->
                // ...
            is UnsortedAction.SetTrips ->
                // ...
            else -> oldState
        }
    }
}
```

Note that we don't need any `setSomething` methods that return an updated copy of an object, Kotlin data classes have the `copy` method that works great with named arguments.

### ViewModel
Structure your `ViewModel`s like this:
```kotlin
class UnsortedViewModel(
    application: Application,
    appDepsProvider: AppDepsProvider
) : AbsViewModel(application, appDepsProvider) {

    private val dispatcher = appDepsProvider.dispatcher
    private val volley = appDepsProvider.volley

    private val _stateLiveData = MutableLiveData<UnsortedState>()
    val stateLiveData: LiveData<UnsortedState> = _stateLiveData

    private val _errorLiveData = SingleEventLiveData<String>()
    val errorLiveData: LiveData<String> = _errorLiveData

    private val _dismissedLiveData = NavigationLiveData<Boolean>()
    val dismissedLiveData: LiveData<Boolean> = _dismissedLiveData

    init {
        // ...
        attachManagedStore(
            UnsortedReducer(),
            listOf(unsortedTripMiddleware),
            UnsortedState(UnsortedViewState.Progress, showHint, null, null)
        ) { newState: UnsortedState ->
            _stateLiveData.value = newState
            newState.errorEvent?.readValue()?.let { _errorLiveData.value = it }
            newState.dismissedEvent?.readValue()?.let { _dismissedLiveData.value = it }
        }
        dispatcher.dispatch(UnsortedAction.Reload)
    }

    override fun onCleared() {
        super.onCleared()
        volley.cancelAll(UnsortedContract.RQ_TAG)
        unsortedTripMiddleware.onCleared()
    }

    // ...
}
```

Note several things here:
* We create a `private` `MutableLiveData` (or any other mutable type) with a `_` prefix in the name and an immutable `public` `LiveData`
* We don't need to manage `Disposable`s manually anymore, `AbsViewModel` has `attachManagedStore` and `attachManagedPreDispatchHook` methods and will `dispose` all the `Disposable`s in it's `onCleared` method.
* In order to prevent `LiveData` invocations with empty `Event`s we use the following pattern:
    ```kotlin
    newState.errorEvent?.readValue()?.let { _errorLiveData.value = it }
    ```

## Database queries
If you need to implement an `AbsDbRequest` use the following pattern to make it more readable:
```kotlin
class LoadFlightsForWearDbRequest(
    private val statusDatabase: Database,
    private val throwableTracker: ThrowableTracker
) : AbsDbRequest<List<NewFlight>>() {

    override fun tryRun(): DbResult<List<NewFlight>> {
        val readableDatabase = statusDatabase.readableDatabase
        val query =
            """
            |SELECT 
            |    ${StatusTable.Column.CARRIER_PREFIXED}, 
            |    ${StatusTable.Column.STATUS_PREFIXED}, 
            |    ${StatusTable.Column.ID_PREFIXED}, 
            |    ${StatusTable.Column.TRIP_ID_PREFIXED}, 
            |    ${StatusTable.Column.ARRIVAL_CODE_PREFIXED}, 
            |    ${StatusTable.Column.DEPARTURE_CODE_PREFIXED}, 
            |    ${StatusTable.Column.NUMBER_PREFIXED}, 
            |    ${StatusTable.Column.ARRIVAL_DATE_PREFIXED}, 
            |    ${StatusTable.Column.DEPARTURE_DATE_PREFIXED}, 
            |    ${StatusTable.Column.ARRIVAL_GATE_PREFIXED}, 
            |    ${StatusTable.Column.DEPARTURE_GATE_PREFIXED}, 
/*                                  ...                                  */
            |FROM 
            |    ${StatusTable.NAME} LEFT OUTER JOIN ${FlightCrowdsourceTable.NAME} 
            |        ON ${StatusTable.Column.ID} = ${FlightCrowdsourceTable.Column.FLIGHT_ID}, 
            |    ${AirportTable.NAME} AS ${AirportTable.DEP}, 
            |    ${AirportTable.NAME} AS ${AirportTable.ARV} 
            |WHERE 
            |    ${StatusTable.Column.ARRIVAL_CODE_PREFIXED} = ${AirportTable.Column.CODE_ARV_PREFIXED} 
            |    AND ${StatusTable.Column.DEPARTURE_CODE_PREFIXED} = ${AirportTable.Column.CODE_DEP_PREFIXED} 
            |ORDER BY ${StatusTable.Column.DEPARTURE_DATE_PREFIXED} DESC;""".trimMargin()
        try {
            readableDatabase.rawQuery(query, null).use { cursor ->
                // ...
                return DbResult.Success(flights)
            }
        } catch (e: Exception) {
            return DbResult.Error(e)
        }
    }
}
```

Note that we're using `trimMargin` method, not `trimIndent` or any other. 

Also note that `use` function is used with `Cursor` - this thing replaces Java's `try-with-resources`.

## Useful extensions
Don't forget that we have a bunch of useful extension methods / properties (the list is not complete, check out different `*Utils.kt` files for more). Also note that we use some of the [KTX extensions](https://developer.android.com/kotlin/ktx/extensions-list) and we can add more of those dependencies if there is a need.
* `Context`
    * ```kotlin
      @ColorInt
      fun Context.getColorCompat(@ColorRes colorId: Int): Int
      ```
    * ```kotlin
      fun Context.getDrawableCompat(@DrawableRes drawableId: Int): Drawable?
      ```
    * ```kotlin
      fun Context.getTintedDrawableCompat(@DrawableRes drawableId: Int, @ColorRes tintColorId: Int): Drawable?
      ```
* `Cursor`
    * ```kotlin
      fun Cursor.optString(columnIndex: Int, fallback: String = ""): String
      ```
    * ```kotlin
      fun Cursor.optInt(columnIndex: Int, fallback: Int = 0): Int
      ```
    * ```kotlin
      fun Cursor.optLong(columnIndex: Int, fallback: Long = 0L): Long
      ```
    * and others
* `String`
    * ```kotlin
      inline fun String?.ifNotNullOrEmpty(block: (String) -> Unit)
      ```
* Time
    * ```kotlin
      fun currentUtcSeconds(): Long
      ```
* `AitaTask`
    * You can run a `VoidAitaTask` in your Kotlin code like this:
        ```kotlin
        voidAitaTask {
            // Perform something on worker thread...
        }.run()
        ```
    * And `AitaTask`:
        ```kotlin
        aitaTask {
            // Perform something on worker thread...
            return@aitaTask ... // Return result of type T
        }.performOnUi { upcomingFlightsCount /* Argument of type T */ ->
            // Perform something on main thread...
        }.run()
        ```
* `AitaJson`
    * ```kotlin
      fun parseAitaJson(jsonStr: String?): AitaJson?
      ```
    * ```kotlin
      fun AitaJson?.stringItems(): List<Pair<String, String>>
      ```
    * ```kotlin
      fun AitaJson?.jsonItems(): List<Pair<String, AitaJson>>
      ```
    * and others
* `AitaJsonArray`
    * ```kotlin
      val AitaJsonArray.lastIndex: Int
      ```
    * ```kotlin
      fun Iterable<Any?>.toJsonArray(): AitaJsonArray
      ```
    * ```kotlin
      fun AitaJsonArray?.stringValues(): List<String>
      ```
    * ```kotlin
      fun AitaJsonArray?.jsonValues(): List<AitaJson>
      ```
    * and others
