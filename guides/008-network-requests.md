[<- Contents](README.md)

# Network requests

## Contents
1. [Simple request returning JSON](#simple-request-returning-json)
1. [Simple request returning parsed object](#simple-request-returning-parsed-object)
1. [`POST` request](#post-request)
1. [Building URL](#building-url)
1. [Blocking execution](#blocking-execution)
1. [Canceling request](#canceling-request)
1. [Multipart request](#multipart-request)
1. [Weak listeners](#weak-listeners)

## Simple request returning JSON
To make a simple request returning `AitaJson` extend `AitaJsonRequest` like this:
```java
public final class MyTestRequest extends AitaJsonRequest {

    public MyTestRequest(final Response.Listener<AitaJson> responseListener,
                         final Response.ErrorListener errorListener) {
        super(Method.GET, AitaContract.REQUEST_PREFIX + "api/trips", responseListener, errorListener);
    }

}
```

It's the most simple implementation. Default headers are provided by the base class. In case of success your `Response.Listener` will receive the resulting `AitaJson`. In case of failure your `Response.ErrorListener` will receive the `VolleyError`.

## Simple request returning parsed object
Passing plain JSONs around is usually a bad practice. In order to parse the resulting `AitaJson` into your data class extend `AitaSimpleRequest<MyType>` like this:
```java
public final class MyTestRequest extends AitaSimpleRequest<MyType> {

    public MyTestRequest(final Response.Listener<MyType> responseListener,
                         final Response.ErrorListener errorListener) {
        super(Method.GET, AitaContract.REQUEST_PREFIX + "api/trips", responseListener, errorListener);
    }

    @Override
    protected MyType responseFromJson(@NonNull final AitaJson json) throws Exception {
        // You can perform much more complex parsing here
        return new MyType(json);
    }

}
```

## `POST` request
### `application/json`
`POST` request with a JSON body can be created like this:
```java
public final class MyPostJsonRequest extends AitaJsonRequest {

    private final AitaJson bodyJson;

    public MyPostJsonRequest(@NonNull final AitaJson bodyJson,
                             @Nullable final Response.Listener<AitaJson> responseListener,
                             @Nullable final Response.ErrorListener errorListener) {
        super(Method.POST, AitaContract.REQUEST_PREFIX + "api/tips", responseListener, errorListener);
        this.bodyJson = bodyJson;
    }

    @Override
    public String getBodyContentType() {
        return AitaContract.CONTENT_TYPE_JSON;
    }

    @Override
    public byte[] getBody() throws AuthFailureError {
        return bodyJson.toString().getBytes(Charset.forName("UTF-8"));
    }

}
```

### `application/x-www-form-urlencoded`
`POST` request with a url-encoded form body can be created like this:
```java
public final class MyPostFormRequest extends AitaJsonRequest {
    
    private final String firstName;
    private final String lastName;

    public MyPostFormRequest(@NonNull final String firstName,
                             @NonNull final String lastName,
                             @Nullable final Response.Listener<AitaJson> responseListener,
                             @Nullable final Response.ErrorListener errorListener) {
        super(Method.POST, AitaContract.REQUEST_PREFIX + "api/user/info", responseListener, errorListener);
        this.firstName = firstName;
        this.lastName = lastName;
    }

    @Override
    protected Map<String, String> getParams() throws AuthFailureError {
        return new HashMap<String, String>(2) {{
            put("first_name", firstName);
            put("last_name", lastName);
        }};
    }

}
```

## Building URL
Sometimes we have to build a URL with params. Use `Uri` for such tasks:
```java
public final class GetTripVolleyRequest extends AitaSimpleRequest<Trip> {

    public GetTripVolleyRequest(final String tripId,
                                final boolean duplicate,
                                final boolean refresh,
                                final Response.Listener<Trip> responseListener,
                                final Response.ErrorListener errorListener) {
        super(
                Method.GET,
                Uri.parse(AitaContract.REQUEST_PREFIX + "api/trips/" + tripId)
                        .buildUpon()
                        .appendQueryParameter("duplicate", duplicate ? "1" : "0")
                        .appendQueryParameter("refresh", refresh ? "1" : "0")
                        .build()
                        .toString(),
                responseListener,
                errorListener
        );
    }

}
```

## Blocking execution
Sometimes we have to run our requests in a blocking way. You can achieve such behavior using this scheme:
```java
final RequestFuture<Trip> future = RequestFuture.newFuture();
final GetTripRequest request = new GetTripRequest(..., future, future);
volley.addRequest(request);
try {
    final Trip trip = future.get(95, TimeUnit.SECONDS);
    // TODO: Handle success here
} catch (final InterruptedException e) {
    LibrariesHelper.logException(e);
    // TODO: Handle error here
} catch (final ExecutionException e) {
    LibrariesHelper.logException(e);
    // TODO: Handle error here
} catch (final TimeoutException e) {
    LibrariesHelper.logException(e);
    // TODO: Handle error here
}
```

## Canceling request
You always should perform a clean-up when your component is being destroyed. For instance, in your `ViewModel` you can achieve such behavior using the `cancelAll` method:
```java
public final class MyViewModel extends ViewModel {
    
    private static final String RQ_TAG = "my_view_model_request_tag";
    
    private final VolleyQueueHelper volley = VolleyQueueHelper.getInstance();
    
    public void doSomething() {
        // Prepare request
        volley.addRequest(request, RQ_TAG);
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        volley.cancelAll(RQ_TAG);
    }

}
```

## Multipart request
Sometimes we have to upload some file along with some data. You can do it like this:
```java
public final class UploadAnalyticsRequest extends AitaMultipartRequest<Void> {

    public UploadAnalyticsRequest(@NonNull @BookingLogger.Product final String product,
                                  @NonNull final File file,
                                  @Nullable final String sessionId,
                                  @NonNull final Response.Listener<Void> responseListener,
                                  @NonNull final Response.ErrorListener errorListener) {
        super(
                Method.POST,
                AitaContract.REQUEST_PREFIX + "api/analytics/booking_log",
                responseListener,
                errorListener
        );
        final String fileName = file.getName();
        final MultipartBody.Builder bodyBuilder = new MultipartBody.Builder()
                .setType(MultipartBody.FORM)
                .addFormDataPart("name", fileName)
                .addFormDataPart("product", product)
                .addFormDataPart("log", "log", RequestBody.create(null, file));
        setRequestBody(bodyBuilder.build());
    }

    @Override
    protected Response<Void> parseNetworkResponse(final NetworkResponse response) {
        return Response.success(null, HttpHeaderParser.parseCacheHeaders(response));
    }

    @Override
    protected Void responseFromJson(@NonNull final AitaJson json) {
        // Nothing to do here
        return null;
    }

}
```

## Weak listeners
Running any request from `Activity` or `Fragment` is a bad practice. Don't do it! However, in legacy code we had to do such things. To prevent memory leaks with such cases use `WeakVolleyResponseListener`:
```java
public final class FlightReviewActivity extends SlideUpActivity {

    // Some other code here uses `new GetFlightReviewsResponseListener(this)` 
    // as Response.Listener and Response.ErrorListener

    static final class GetFlightReviewsResponseListener extends WeakVolleyResponseListener<FlightReviewActivity, List<FlightReview>> {
        GetFlightReviewsResponseListener(@NonNull final FlightReviewActivity self) {
            super(self);
        }

        @Override
        public void onResponse(@Nullable final FlightReviewActivity self, @Nullable final List<FlightReview> response) {
            if (self == null) {
                return;
            }
            self.swipeRefreshLayout.setRefreshing(false);
            if (response == null) {
                return;
            }
            self.flightReviews = response;
            self.updateList(self.flightReviews);
        }

        @Override
        public void onErrorResponse(@Nullable final FlightReviewActivity self, @Nullable final VolleyError error) {
            LibrariesHelper.logException(error);
            if (self == null) {
                return;
            }
            self.swipeRefreshLayout.setRefreshing(false);
        }
    }

}
```
