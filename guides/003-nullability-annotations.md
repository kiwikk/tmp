[<- Contents](README.md)

# Nullability annotations
Using nullability annotations is a good tone and is always encouraged and appreciated. Use them for fields, return values and method params. We use `android.support.annotation.NonNull` and `android.support.annotation.Nullable`. However, there are a few simple rules:
* If you don't see any nullability annotation, most probably you should check that value for `null`
* If you want to place the `@NonNull` annotation, you should be able to prove that `null` will never occur. Prove here means that `if (x == null) { throw new WhateverException(); }` can be added anywhere, and it will never be executed at runtime.