[<- Contents](README.md)

# Dialogs

## Bottom sheet dialogs

### Parent
- If you working with new flux architecture you should extend 
`AbsArchBottomSheetDialogFragment`

- If you working without new flux architecture you should extend
`AbsBottomSheetDialogFragment`

- If you want to listen peek height discuss it with the team and see
`AbsListenablePeekHeightDialogFragment`

### Xml
```xml
<!-- Outer FrameLayout is required -->
<FrameLayout 
    xmlns:android="http://schemas.android.com/apk/res/android"
    android:layout_width="match_parent"
    android:layout_height="wrap_content">

    <FrameLayout
      style="@style/BottomSheetContainer">

        <LinearLayout
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:orientation="vertical">

           <LinearLayout
               android:id="@+id/title_block"
               android:layout_width="match_parent"
               android:layout_height="wrap_content"
               android:layout_weight="0"
               android:orientation="vertical"
               style="@style/BottomSheetHeaderBlock">

               <View
                   android:id="@+id/swiper"
                   style="@style/BottomSheetSwiper"/> 
        
                <!-- Optional Title -->
            </LinearLayout>            

                <!-- NestedScrollView or RecyclerView -->
                <YourScrollableView 
                    android:id="@+id/scroll_view"
                    android:layout_weight="1"
                    android:layout_width="match_parent"
                    android:layout_height="wrap_content"/>

            <!-- Optional Footer -->
           <YourViewGroup
               android:id="@+id/button_block"
               android:layout_width="match_parent"
               android:layout_height="wrap_content"
               android:layout_weight="0"
               style="@style/BottomSheetFooterBlock"/>
        </LinearLayout>
    </FrameLayout>
</FrameLayout>
```

### Styles
```xml
style="@style/BottomSheetContainer"
style="@style/BottomSheetSwiper"
style="@style/BottomSheetHeaderBlock"
style="@style/BottomSheetFooterBlock"
```

### DialogFragment
```Java
public final class MyDialogFragment extends AbsArchBottomSheetDialogFragment {

    public static MyDialogFragment newInstance() {
        return new MyDialogFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull final LayoutInflater inflater,
                             @Nullable final ViewGroup container,
                             @Nullable final Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_bottom_sheet_my, container, false);
    }

    @Override
    public void onStart() {
        super.onStart();
        final View view = requireView();
        final Context context = requireContext();
        final AppViewModelFactory appViewModelFactory = getAppViewModelFactory();
        final ViewModelProvider viewModelProvider = new ViewModelProvider(this, appViewModelFactory);
        final MyDialogViewModel viewModel = viewModelProvider.get(MyDialogViewModel.class);

        // Required call
        // Save scrollHeightHelper in variable if your dialog 
        // can change height without configuration changes.
        // Example - if you make some view visibility gone 
        // on button clicked call scrollHeightHelper.updateElevations()
        final ScrollBottomSheetHelper scrollHeightHelper = new ScrollBottomSheetHelper(
            (BottomSheetDialog) requireDialog(),
            scrollView,
            buttonBlock,
            titleBlock
        );

        // Optional
        viewModel.reset(new MyDialogViewModel.Input(data));
        viewModel.someLiveData().observe(...);
    }

    @Override
    protected String getAnalyticsScreenName() {
        return "MyDialogFragment";
    }

    @Override
    protected int getRequestCode() {
        // or return requireArguments().getInt(ARG_REQUEST_CODE) 
        return STATIC_REQUEST_CODE;
    }

    @Override
    protected boolean isExpandFull() {
        // usually true if your dialog should show as much as possible when starts
        return needToExpandFullHeightWhenDialogIsOpen;
    }
}
```

### How to show 
```java
    MyDialogFragment.newInstance().showAfterKeyboardHides(fragmentManager, "my-dialog", activity);
```

### FAQ
Why do we need it?

1) All logic is moved to onStart because BottomSheetDialogFragment have specific lifecycle differences with Fragment
2) ScrollBottomSheetHelper updates elevations on scroll event and dismisses dialog when it's fully hided
3) Root FrameLayout handle side clicks on a tablet to dismiss the dialog
4) BottomSheetContainer style handle clicks to not pass it through itself, makes a smaller width on a tablet, makes in the center on tablets
5) BottomSheetSwiper have needed margins and bg
6) BottomSheetHeaderBlock, BottomSheetFooterBlock have stateListAnimator and needed bg to make elevation works
7) Show with showAfterKeyboardHides() - because BottomSheetDialog has a bug and doesn't relayout itself when the keyboard is gone
8) getRequestCode() is need not only the way you send result data, 
but also it used to understanding need to reset expandedStateViewModel or not
9) isExpandFull() if set false bottomSheetDialog will decide show dialog in expanded or half_expanded state in dependency of content height