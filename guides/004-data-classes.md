[<- Contents](README.md)

# Data classes
By data classes we mean classes that are responsible for holding data of some entity (airline, airport, flight, trip, etc.) and computing something on top of that data. They (**optionally**)...
* ... have getters
* ... have instance-creating setters
* ... know how to read/write themselves from/to `Parcel`
* ... know how to read/write themselves from/to `AitaJson`
* ... know how to read/write themselves from/to `Cursor`/`ContentValues`

## Contents
1. [Basics](#basics)
1. [Setters](#setters)
1. [`equals`, `hashCode` and `toString`](#equals-hashcode-and-tostring)
1. [Sealed classes and `Enum`s](#sealed-classes-and-enums)
1. [`Parcelable`](#parcelable)
1. [JSON](#json)
1. [`Cursor` and `ContentValues`](#cursor-and-contentvalues)
1. [Overall layout](#overall-layout)

## Basics
Typical data class is built by the following rules:
* It's `final` - inheritance usually has no use in data classes
* All it's fields are `private final`
```java
public final class Airport {

    private final String code;
    private final String name;
    private final String city;

    public Airport(final String code, final String name, final String city) {
        this.code = code;
        this.name = name;
        this.city = city;
    }

}
```

Getters are added as usual (by using `Generate -> Getter` in Intellij IDEA) if necessary:
```java
public final class Airport {

    /* FIELDS */

    public Airport(...) {...}

    public String getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

    public String getCity() {
        return city;
    }

}
```

Computational methods that contain some logic are placed before the getters:
```java
public final class Airport {

    /* FIELDS */

    public Airport(...) {...}

    @NonNull
    public String getSearchString() {
        return code + " " + name + " " + city;
    }

    /* GETTERS */

}
```

## Setters
Common setters are not allowed. Any state mutations are bad and should be avoided by all means. Each data class object should be immutable. So, you cannot do this:
```java
public final class Airport {

    /* FIELDS */

    public Airport(...) {...}

    /* COMPUTATIONAL METHODS */

    /* GETTERS */

    // Forbidden!
    public void setCode(@NonNull final String code) {
        this.code = code;
    }

}
```
But you can do this:
```java
public final class Airport {

    /* FIELDS */

    public Airport(...) {...}

    /* COMPUTATIONAL METHODS */

    /* GETTERS */

    // Nice!
    @NonNull
    public Airport setCode(@NonNull final String newCode) {
        return new Airport(newCode, this.name, this.city);
    }

}
```

## `equals`, `hashCode` and `toString`
`equals`, `hashCode` and `toString` methods are generated via Intellij IDEA `Generate -> equals() and hashCode() / toString()` and placed below the getters and setters:
```java
public final class Airport {

    /* FIELDS */

    public Airport(...) {...}

    /* COMPUTATIONAL METHODS */

    /* GETTERS */

    /* SETTERS */

    @Override
    @SuppressWarnings({"SimplifiableIfStatement", "RedundantIfStatement"})
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        final Airport airport = (Airport) o;

        if (code != null ? !code.equals(airport.code) : airport.code != null) {
            return false;
        }
        if (name != null ? !name.equals(airport.name) : airport.name != null) {
            return false;
        }
        if (city != null ? !city.equals(airport.city) : airport.city != null) {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode() {
        int result = code != null ? code.hashCode() : 0;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (city != null ? city.hashCode() : 0);
        return result;
    }

    @Override
    @NonNull
    public String toString() {
        return "Airport{" +
                "code='" + code + '\'' +
                ", name='" + name + '\'' +
                ", city='" + city + '\'' +
                '}';
    }

}
```

## Sealed classes and `Enum`s
Java, unlike Kotlin, doesn't have [sealed classes](https://kotlinlang.org/docs/reference/sealed-classes.html) to nicely express the `x is of type T, while T can be T1 xor T2 xor T3...` situation. 

`Enum`s:
1. Cannot be instantiated 
1. In some cases have performance hit 
1. Increase the `NullPointerException` risk:
```java
enum Type { FOOTER, HEADER, ROW }

final Type t = ...;

// Crashes with NPE if t == null
switch (t) {
    case FOOTER:
        ...
        break;
    case HEADER:
        ...
        break;
    case ROW:
        ...
        break;
}

// Correct way with an ugly nesting level
if (t == null) {
    ...
} else {
    switch (t) {
        case FOOTER:
            ...
            break;
        case HEADER:
            ...
            break;
        case ROW:
            ...
            break;
    }
}
```

With all that said, we use an abstract class with package-private constructor and some kind of `enum Type`.

This abstract class looks like this:
```java
public abstract class AbsMyTripsCell {

    public enum ViewType {
        UNSORTED,
        TITLE,
        ARRIVE_AT,
        ...
    }

    @NonNull
    private final ViewType viewType;

    AbsMyTripsCell(@NonNull final ViewType viewType) {
        this.viewType = Throw.ifNull(viewType);
    }

    @NonNull
    public ViewType getViewType() {
        return viewType;
    }

    @Override
    @SuppressWarnings({"SimplifiableIfStatement", "RedundantIfStatement"})
    public boolean equals(final Object o) { ... }

    @Override
    public int hashCode() { ... }

    @Override
    @NonNull
    public String toString() { ... }

}
```

And the children:
```java
public final class UnsortedMyTripsCell extends AbsMyTripsCell {

    private final int unsortedTripsCount;

    public UnsortedMyTripsCell(final int unsortedTripsCount) {
        super(ViewType.UNSORTED);
        this.unsortedTripsCount = Throw.ifZeroOrNegative(unsortedTripsCount);
    }

    public int getUnsortedTripsCount() {
        return unsortedTripsCount;
    }

    @Override
    @SuppressWarnings({"SimplifiableIfStatement", "RedundantIfStatement"})
    public boolean equals(final Object o) { ... }

    @Override
    public int hashCode() { ... }

    @Override
    @NonNull
    public String toString() { ... }

}
```

```java
public final class TitleMyTripsCell extends AbsMyTripsCell {

    @Nullable
    private final UnevenWeightTitle unevenWeightTitle;
    @Nullable
    private final String subtitle;
    @Nullable
    private final String timeText;

    public TitleMyTripsCell(@Nullable final UnevenWeightTitle unevenWeightTitle,
                            @Nullable final String subtitle,
                            @Nullable final String timeText) {
        super(ViewType.TITLE);
        this.unevenWeightTitle = unevenWeightTitle;
        this.subtitle = subtitle;
        this.timeText = timeText;
    }

    @Nullable
    public UnevenWeightTitle getUnevenWeightTitle() {
        return unevenWeightTitle;
    }

    @Nullable
    public String getSubtitle() {
        return subtitle;
    }

    @Nullable
    public String getTimeText() {
        return timeText;
    }

    @Override
    @SuppressWarnings({"SimplifiableIfStatement", "RedundantIfStatement"})
    public boolean equals(final Object o) { ... }

    @Override
    public int hashCode() { ... }

    @Override
    @NonNull
    public String toString() { ... }

}
```

```java
public final class ArriveAtMyTripsCell extends AbsMyTripsCell {

    @Nullable
    private final String arrivalText;
    @Nullable
    private final String stopDurationText;
    private final boolean dividerShown;
    @Nullable
    private final String terminalChangeText;

    public ArriveAtMyTripsCell(@Nullable final String arrivalText,
                               @Nullable final String stopDurationText,
                               final boolean dividerShown,
                               @Nullable final String terminalChangeText) {
        super(ViewType.ARRIVE_AT);
        this.arrivalText = arrivalText;
        this.stopDurationText = stopDurationText;
        this.dividerShown = dividerShown;
        this.terminalChangeText = terminalChangeText;
    }

    @Nullable
    public String getArrivalText() {
        return arrivalText;
    }

    @Nullable
    public String getStopDurationText() {
        return stopDurationText;
    }

    public boolean isDividerShown() {
        return dividerShown;
    }

    @Nullable
    public String getTerminalChangeText() {
        return terminalChangeText;
    }

    @Override
    @SuppressWarnings({"SimplifiableIfStatement", "RedundantIfStatement"})
    public boolean equals(final Object o) { ... }

    @Override
    public int hashCode() { ... }

    @Override
    @NonNull
    public String toString() { ... }

}
```

And now you can use this thing like that:
```java
final AbsMyTripsCell cell = ...;
final AbsMyTripsCell.ViewType viewType = cell.getViewType();
switch (viewType) {
    case UNSORTED:
        final UnsortedMyTripsCell unsortedCell = (UnsortedMyTripsCell) cell;
        // Do something with unsortedCell
    case TITLE:
        final TitleMyTripsCell titleCell = (TitleMyTripsCell) cell;
        // Do something with titleCell
    case ARRIVE_AT:
        final ArriveAtMyTripsCell arriveCell = (ArriveAtMyTripsCell) cell;
        // Do something with arriveCell 
    // Other cases...
    default:
        throw new IllegalArgumentException("Unknown ViewType: " + viewType);
}
```

Also note that `IllegalArgumentException` should always be thrown in order to find potential errors in the early development stage

## `Parcelable`
Use [Android Parcelable code generator](https://plugins.jetbrains.com/plugin/7332-android-parcelable-code-generator) Intellij IDEA plugin for `Parcelable` boilerplate generation, place it all below the `equals`, `hashCode` and `toString` methods:
```java
public final class Airport implements Parcelable {

    /* FIELDS */

    public Airport(...) {...}

    /* COMPUTATIONAL METHODS */

    /* GETTERS */

    /* SETTERS */

    /* EQUALS, HASHCODE & TOSTRING */

    @Override
    public int describeContents() { return 0; }

    @Override
    public void writeToParcel(final Parcel dest, final int flags) {
        dest.writeString(this.code);
        dest.writeString(this.name);
        dest.writeString(this.city);
    }

    protected Airport(final Parcel in) {
        this.code = in.readString();
        this.name = in.readString();
        this.city = in.readString();
    }

    public static final Parcelable.Creator<Airport> CREATOR = new Parcelable.Creator<Airport>() {
        @Override
        public Airport createFromParcel(final Parcel source) {return new Airport(source);}

        @Override
        public Airport[] newArray(final int size) {return new Airport[size];}
    };

}
```

## JSON
To parse an object from `AitaJson` we use class constructors. To create `AitaJson` from an object we use `AitaJson toJson()` method. Place these two after your regular constructors and before your computational methods:
```java
public final class Airport {

    /* FIELDS */

    public Airport(...) {...}

    public Airport(@NonNull final AitaJson json) {
        this.code = json.optString("code");
        this.name = json.optString("name");
        this.city = json.optString("city");
    }

    @NonNull
    public AitaJson toJson() {
        return new AitaJson()
                .put("code", code)
                .put("name", name)
                .put("city", city);
    }

    /* COMPUTATIONAL METHODS */

    /* GETTERS */

    /* SETTERS */

    /* EQUALS, HASHCODE & TOSTRING */

    /* PARCELABLE METHODS AND FIELDS */

}
```
Live Template for `toJson()` method can be found [here](011-downloads.md#intellij-idea-live-templates).

## `Cursor` and `ContentValues`
We use `ContentValues toContentValues()` method to write an object to `ContentValues`. To read an object from a `Cursor` we use a constructor `constructor(Cursor cursor, CursorColumnIndexHolder holder)`. Put them after your JSON-related constructor and method and before your computational methods. `CursorColumnIndexHolder` here is a class that loads and holds indexes of `Cursor` columns. See example for details:
```java
public final class Airport implements Parcelable {

    /* FIELDS */

    public Airport(...) {...}

    /* JSON CONSTRUCTOR AND METHOD */

    public Airport(@NonNull final Cursor cursor, @NonNull final CursorColumnIndexHolder holder) {
        code = cursor.getString(holder.codeColIndex);
        name = cursor.getString(holder.nameColIndex);
        city = cursor.getString(holder.cityColIndex);
    }

    @NonNull
    public ContentValues toContentValues() {
        final ContentValues cv = new ContentValues(3);
        cv.put(AirportContract.codeKey, code);
        cv.put(AirportContract.nameKey, name);
        cv.put(AirportContract.cityKey, city);
        return cv;
    }

    public static final class CursorColumnIndexHolder {
        final int codeColIndex;
        final int nameColIndex;
        final int cityColIndex;

        public CursorColumnIndexHolder(@NonNull final Cursor cursor) {
            codeColIndex = cursor.getColumnIndex(AirportContract.codeKey);
            nameColIndex = cursor.getColumnIndex(AirportContract.nameKey);
            cityColIndex = cursor.getColumnIndex(AirportContract.cityKey);
        }
    }

    /* COMPUTATIONAL METHODS */

    /* GETTERS */

    /* SETTERS */

    /* EQUALS, HASHCODE & TOSTRING */

    /* PARCELABLE METHODS AND FIELDS */

}
```

Live Template for `toContentValues()` method and `CursorColumnIndexHolder` class can be found [here](011-downloads.md#intellij-idea-live-templates).

## Overall layout
Finally, we're able to define a layout of a general data class:
```java
public final class Airport implements Parcelable {

    /* CONSTANTS */

    /* FIELDS */

    public Airport(...) {...}

    /* JSON CONSTRUCTOR AND METHOD */

    /* DATABASE-RELATED CONSTRUCTOR, METHOD & COLUMN INDEX HOLDER */

    /* COMPUTATIONAL METHODS */

    /* GETTERS */

    /* SETTERS */

    /* EQUALS, HASHCODE & TOSTRING */

    /* PARCELABLE METHODS AND FIELDS */

}