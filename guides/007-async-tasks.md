[<- Contents](README.md)

# Async tasks
Right now we're using a primitive `AsyncTask`-like wrapper on top of `Executor`s. It has several disadvantages but it's fast and simple. Perhaps in the future we'll develop something better.

## Contents
1. [`AitaTask`](#aitatask)
1. [`VoidAitaTask`](#voidaitatask)
1. [`WeakAitaTask`](#weakaitatask)

## `AitaTask`
You can perform some actions on a background thread in your `ViewModel` or some other context that doesn't leak `Context` like this:
```java
new AitaTask<ProfileStats>() {
    @NonNull
    @Override
    public ProfileStats runOnBackgroundThread() {
        return new ProfileStats(...);
    }

    @Override
    public void runOnUiThread(@NonNull final ProfileStats profileStats) {
        loadBadges(profileStats);
    }
}.run();
```

## `VoidAitaTask`
If you don't need the result of your task, use the following scheme:
```java
new VoidAitaTask() {
    @Override
    public void performOnBackgroundThread() {
        // Do some hard work here
    }
}.run();
```

## `WeakAitaTask`
Running async tasks from `Activity` or `Fragment` is a bad practice. Don't do this! However, we had to perform such tasks in the legacy code. Use the following scheme to prevent memory leaks:
```java
public final class AirportActivity extends SlideUpActivity {

    // Something happens here

    static final class LoadAirportTask extends WeakAitaTask<AirportActivity, Airport> {
        private final String airportCode;
        private final FlightDataBaseHelper fDbHelper;

        LoadAirportTask(@NonNull final AirportActivity self, @NonNull final String airportCode) {
            super(self);
            this.airportCode = airportCode;
            this.fDbHelper = FlightDataBaseHelper.getInstance();
        }

        @Nullable
        @Override
        public Airport runOnBackgroundThread(@Nullable final AirportActivity self) {
            try {
                return fDbHelper.loadAirport(airportCode);
            } catch (final Exception e) {
                LibrariesHelper.logException(e);
                return null
            }
        }

        @Override
        public void runOnUiThread(@Nullable final AirportActivity self, @Nullable final Airport airport) {
            if (self == null) {
                return;
            }
            // TODO: Handle the result
        }
    }

}
```
