[<- Contents](README.md)

# Code style

## Contents
1. [Basic conventions and frequent mistakes](#basic-conventions-and-frequent-mistakes)
    1. [TODO comments](#todo-comments)
    1. [Logging](#logging)
    1. [Lambdas](#lambdas)
    1. [Multiple invocations of the same method](#multiple-invocations-of-the-same-method)
    1. [`String` constants](#string-constants)
    1. [`null` and `isEmpty` checks](#null-and-isempty-checks)
    1. [Early `return`](#early-return)
    1. [Logging exceptions](#logging-exceptions)
    1. [`boolean` fields naming](#boolean-fields-naming)
    1. [`View` fields naming](#view-fields-naming)
    1. [Basic `Activity` conventions](#basic-activity-conventions)
    1. [Basic `Fragment` conventions](#basic-fragment-conventions)
    1. [Method declaration wrapping](#method-declaration-wrapping)
    1. [Method invocation wrapping](#method-invocation-wrapping)
    1. [As immutable as possible](#as-immutable-as-possible)
    1. [As method-local as possible](#as-method-local-as-possible)
    1. [(A la) sealed classes factory methods naming conventions](#a-la-sealed-classes-factory-methods-naming-conventions)
    1. [`AitaJson` and `AitaJsonArray` variables naming conventions](#aitajson-and-aitajsonarray-variables-naming-conventions)
    1. [`try-with-resources`](#try-with-resources)
    1. [Handling clicks](#handling-clicks)
1. [Checkstyle rules](#checkstyle-rules)
    1. [Annotations](#annotations)
        1. [AnnotationLocation](#annotationlocation)
        1. [AnnotationUseStyle](#annotationusestyle)
    1. [Block Checks](#block-checks)
        1. [AvoidNestedBlocks](#avoidnestedblocks)
        1. [EmptyBlock](#emptyblock)
        1. [EmptyCatchBlock](#emptycatchblock)
        1. [LeftCurly](#leftcurly)
        1. [NeedBraces](#needbraces)
        1. [RightCurlySame](#rightcurlysame)
        1. [RightCurlyAloneOrSingleLine](#rightcurlyaloneorsingleline)
    1. [Class Design](#class-design)
        1. [FinalClass](#finalclass)
        1. [HideUtilityClassConstructor](#hideutilityclassconstructor)
        1. [InnerTypeLast](#innertypelast)
        1. [InterfaceIsType](#interfaceistype)
        1. [MutableException](#mutableexception)
        1. [OneTopLevelClass](#onetoplevelclass)
        1. [ThrowsCount](#throwscount)
        1. [VisibilityModifier](#visibilitymodifier)
    1. [Coding](#coding)
        1. [ArrayTrailingComma](#arraytrailingcomma)
        1. [CovariantEquals](#covariantequals)
        1. [DeclarationOrder](#declarationorder)
        1. [DefaultComesLast](#defaultcomeslast)
        1. [EmptyStatement](#emptystatement)
        1. [EqualsAvoidNull](#equalsavoidnull)
        1. [EqualsHashCode](#equalshashcode)
        1. [ExplicitInitialization](#explicitinitialization)
        1. [FallThrough](#fallthrough)
        1. [FinalLocalVariable](#finallocalvariable)
        1. [IllegalThrows](#illegalthrows)
        1. [IllegalToken](#illegaltoken)
        1. [IllegalType](#illegaltype)
        1. [InnerAssignment](#innerassignment)
        1. [MagicNumber](#magicnumber)
        1. [MissingSwitchDefault](#missingswitchdefault)
        1. [ModifiedControlVariable](#modifiedcontrolvariable)
        1. [MultipleStringLiterals](#multiplestringliterals)
        1. [MultipleVariableDeclarations](#multiplevariabledeclarations)
        1. [NestedForDepth](#nestedfordepth)
        1. [NestedIfDepth](#nestedifdepth)
        1. [NestedTryDepth](#nestedtrydepth)
        1. [NoClone](#noclone)
        1. [NoFinalizer](#nofinalizer)
        1. [OneStatementPerLine](#onestatementperline)
        1. [OverloadMethodsDeclarationOrder](#overloadmethodsdeclarationorder)
        1. [PackageDeclaration](#packagedeclaration)
        1. [ParameterAssignment](#parameterassignment)
        1. [RequireThis](#requirethis)
        1. [SimplifyBooleanExpression](#simplifybooleanexpression)
        1. [SimplifyBooleanReturn](#simplifybooleanreturn)
        1. [StringLiteralEquality](#stringliteralequality)
        1. [UnnecessaryParentheses](#unnecessaryparentheses)
        1. [UnnecessarySemicolonAfterTypeMemberDeclaration](#unnecessarysemicolonaftertypememberdeclaration)
        1. [UnnecessarySemicolonInEnumeration](#unnecessarysemicoloninenumeration)
        1. [UnnecessarySemicolonInTryWithResources](#unnecessarysemicolonintrywithresources)
    1. [Imports](#imports)
        1. [AvoidStarImport](#avoidstarimport)
        1. [AvoidStaticImport](#avoidstaticimport)
        1. [RedundantImport](#redundantimport)
        1. [UnusedImports](#unusedimports)
    1. [Javadoc Comments](#javadoc-comments)
        1. [InvalidJavadocPosition](#invalidjavadocposition)
        1. [JavadocBlockTagLocation](#javadocblocktaglocation)
    1. [Miscellaneous](#miscellaneous)
        1. [ArrayTypeStyle](#arraytypestyle)
        1. [CommentsIndentation](#commentsindentation)
        1. [FinalParameters](#finalparameters)
        1. [Indentation](#indentation)
        1. [OuterTypeFilename](#outertypefilename)
        1. [TodoComment](#todocomment)
        1. [UncommentedMain](#uncommentedmain)
        1. [UpperEll](#upperell)
    1. [Modifiers](#modifiers)
        1. [ModifierOrder](#modifierorder)
        1. [RedundantModifier](#redundantmodifier)
    1. [Naming conventions](#naming-conventions)
        1. [AbbreviationAsWordInName](#abbreviationaswordinname)
        1. [AbstractClassName](#abstractclassname)
        1. [ClassTypeParameterName](#classtypeparametername)
        1. [ConstantName](#constantname)
        1. [InterfaceTypeParameterName](#interfacetypeparametername)
        1. [LambdaParameterName](#lambdaparametername)
        1. [LocalFinalVariableName](#localfinalvariablename)
        1. [LocalVariableName](#localvariablename)
        1. [MemberName](#membername)
        1. [MethodName](#methodname)
        1. [MethodTypeParameterName](#methodtypeparametername)
        1. [PackageName](#packagename)
        1. [ParameterName](#parametername)
        1. [StaticVariableName](#staticvariablename)
        1. [TypeName](#typename)
    1. [Size violations](#size-violations)
        1. [OuterTypeNumber](#outertypenumber)
        1. [LineLength](#linelength)
    1. [Whitespace](#whitespace)
        1. [EmptyForInitializerPad](#emptyforinitializerpad)
        1. [EmptyForIteratorPad](#emptyforiteratorpad)
        1. [EmptyLineSeparator](#emptylineseparator)
        1. [GenericWhitespace](#genericwhitespace)
        1. [MethodParamPad](#methodparampad)
        1. [NoLineWrap](#nolinewrap)
        1. [NoWhitespaceAfterAndAllowLineBreaks](#nowhitespaceafterandallowlinebreaks)
        1. [NoWhitespaceAfterAndNoLineBreaks](#nowhitespaceafterandnolinebreaks)
        1. [NoWhitespaceBefore](#nowhitespacebefore)
        1. [OperatorWrap](#operatorwrap)
        1. [ParenPad](#parenpad)
        1. [SeparatorWrapNewLine](#separatorwrapnewline)
        1. [SeparatorWrapEndOfLine](#separatorwrapendofline)
        1. [SingleSpaceSeparator](#singlespaceseparator)
        1. [TypecastParenPad](#typecastparenpad)
        1. [WhitespaceAfter](#whitespaceafter)
        1. [WhitespaceAround](#whitespacearound)
        1. [FileTabCharacter](#filetabcharacter)


## Basic conventions and frequent mistakes

Most of these rules aren't checked automatically yet. Nevertheless, they must be obeyed.


### TODO comments

We use a number of special TODO comments while developing something, but none of them should stay in the code when the work is done. So feel free to use them while coding something, but remove all of them from the code when your work is ready for review.

```java
// TODO: TASK: ...
// Used as a regular TODO so we don't forget something
// 'todo' or 'todot' live template


// TODO: REMOVE: ...
// Used to remind us to remove some piece of debug code
// 'todor' live template


// TODO: TRANSLATE
// Used to remind us to create a string resource for a string and to upload it to the translations system
// 'todotr' live template


// TODO: UNCOMMENT
// Used to remind us to uncomment some piece of code
// 'todou' live template
```

All of these are available as [Intellij IDEA Live Templates](011-downloads.md#intellij-idea-live-templates).


### Logging

#### Temporal logs

First of all, don't use `android.util.Log.*` methods directly. When you need some temporal log statement for debug purposes, use `MainHelper.log(<tag>, <text>)`. For convenience, use the `log` [Live Template](011-downloads.md#intellij-idea-live-templates):

```java
MainHelper.log("CurrentClassName", "currentMethodName: "); // TODO: REMOVE
```

#### Permanent logs

Sometimes we have to cover our feature with permanent logs for faster diagnostics of future problems or simply for better understanding of the whole system. We can do that in a number of ways.

For performance-critical cases (e.g. `OkHttp` `Interceptor`) use the verbose form (it allows to toggle logs quickly and to fully strip them out in release builds):
```java
public static final boolean LOG_ON = BuildConfig.DEBUG;

private void someMethod() {
    // Some smarty pants code...
    if (LOG_ON) {
        MainHelper.log("MyTag", "my message");
    }
    // More smarty pants code...
}
```

In general you don't need to use that verbose form and should use the dummy logger class:
```java
public final class MyFeatureLogger {

    private static final boolean LOG_ON = BuildConfig.DEBUG;

    public static void log(@NonNull final String msg) {
        if (LOG_ON) {
            MainHelper.log("MyFeature", msg);
        }
    }

    private MyFeatureLogger() {}

}

// Usage:
MyFeatureLogger.log("We're here");
```

If you need to log something that is expensive to turn into a string, use the following pattern:
```java
public final class MyFeatureLogger {

    private static final boolean LOG_ON = BuildConfig.DEBUG;

    public static void log(@NonNull final Line line) {
        if (LOG_ON) {
            MainHelper.log("MyFeature", line.get());
        }
    }

    public interface Line {
        @NonNull
        String get();
    }
    
    private MyFeatureLogger() {}

}

// Usage:
MyFeatureLogger.log(() -> "This value is " + expensiveToStringValue);
```


### Lambdas

In almost all cases lambdas are preferred to anonymous classes. But there are cases in which short lambda form reduces readability. And in such cases we should specify the type of the lambda params. 

For example, everyone knows that `View.OnClickListener.onClick` method takes a single `View` parameter. So there is no reason to specify the type explicitly. This form is OK:
```java
button.setOnClickListener(v -> {
    // Handling goes here
});
``` 

Sometimes it's unobvious, what type your params have (e.g. `androidx.lifecycle.Observer`). In such cases we should explicitly specify the type of params:
```java
myViewModel.getState().observe(viewLifecycleOwner, (@Nullable final MyState myState) -> {
    // Handling goes here
});
```

You should always look at your lambdas and check whether it's obvious (without IDE, sometimes code is being read on Github, you know) what types your params have or not.


### Multiple invocations of the same method

One of the common mistakes we see again and again is calling the same getter-like method in a single scope multiple times. Just don't do that.

#### ✅ This is nice:

```java
final FragmentActivity activity = getActivity();
if (activity == null) {
    return;
}
activity.doThisAndThat();
// More stuff goes here...
doSomethingWithActivity(activity);
```


#### ❌ And this is freaked up:

```java
if (getActivity() == null) {
    return;
}
getActivity().doThisAndThat();
// More stuff goes here...
doSomethingWithActivity(getActivity());
```


### `String` constants

Sometimes it's unclear, whether a `String` constant field should be created or not. The rule is simple:
1. If it's used in a **single** file 1 or 2 times (e.g. `constructor(AitaJson)` and `AitaJson toJson()` method) - there's usually no need in a constant field (however, you can create one if it improves readability)
1. In other cases you should extract a constant field


### `null` and `isEmpty` checks

Truth be told, there are a lot of nullable variables and methods on our hands. And we should check them. Moreover, most of Android SDK methods are nullable (absence of the `@Nullable` annotation means that it's nullable as well). Lint is helpful here, but not always. So don't forget to check references for `null`, `List`s for emptiness (with `.isEmpty()`) and `String`s for badness (with `MainHelper.isDummyOrNull()`).

#### ✅ Good:

```java
final FragmentActivity activity = getActivity();
if (activity == null) {
    return;
}

final FragmentManager fragmentManager = getSupportFragmentManager();
if (fragmentManager == null) {
    return;
}

final AitaJson someJson = json.optJson("some");
if (someJson == null) {
    return;
}

final List<MyCell> cells = state.getCells();
if (cells == null || cells.isEmpty()) {
    return;
}

final String name = userJson.optString("name");
if (MainHelper.isDummyOrNull(name)) {
    return;
}

// And so on and so forth...
```

Also check out the [Nullability annotations guide](003-nullability-annotations.md).


### Early `return`

In your methods always try to `return` as early as possible. 

#### ✅ Do like this:

```java
@NonNull
public String onSomethingHappened(@Nullable final Something something) {
    if (something == null) {
        return "something is null";
    }
    final Another another = anotherLiveData.getValue();
    if (another == null) {
        return "another is null";
    }
    final int index = myList.indexOf(something);
    if (index < 0) {
        return "index is wrong";
    }
    final int anotherIndex = myList.indexOf(another);
    if (anotherIndex < 0) {
        return "another index is wrong";
    }
    return "OK";
}
```


#### ❌ Never write code like this:

```java
@NonNull
public String onSomethingHappened(@Nullable final Something something) {
    if (something != null) {
        final Another another = anotherLiveData.getValue();
        if (another != null) {
            final int index = myList.indexOf(something);
            if (index >= 0) {
                final int anotherIndex = myList.indexOf(another);
                if (anotherIndex >= 0) {
                    return "OK";
                } else {
                    return "another index is wrong";
                }
            } else {
                return "index is wrong";
            }
        } else {
            return "another is null";
        }
    } else {
        return "something is null";
    }
}
```


### Logging exceptions

For caught exceptions:

#### ✅ Do this:

```java
} catch (final SomeException e) {
    LibrariesHelper.logException(e); // Almost always
    // OR
    e.printStackTrace(); // Rare case
}
```


#### ❌ And never this:

```java
} catch (final SomeException e) {
    Crashlytics.logException(e); // Ouch!
}
```


### `boolean` fields naming

Don't use `is` prefix in the names of your `boolean` fields. It makes them clash with getters. Simply use words.

#### ✅ Good:

```java
private final boolean same;
private final boolean read;
private final boolean checked;

public boolean isSame() {
    return same;
}

public boolean isRead() {
    return read;
}

public boolean isChecked() {
    return checked;
}
```


#### ❌ Bad:

```java
private final boolean isSame;
private final boolean isRead;
private final boolean isChecked;

public boolean isSame() {
    return isSame;
}

public boolean isRead() {
    return isRead;
}

public boolean isChecked() {
    return isChecked;
}
```


### `View` fields naming

Use the `View` descendant class name as a postfix for such fields.

#### ✅ Good:

```java
private final TextView nameTextView;
private final ImageView photoImageView;
private final RecyclerView achievementsRecyclerView;
```


#### ❌ Bad:

```java
private final TextView name;
private final ImageView photo;
private final RecyclerView achievements;
```


### Basic `Activity` conventions

Always create a `makeIntent` method for any `Activity`, use `EXTRA_` prefix for the constants with input params and `STATE_` prefix for state saving:

```java
public final class MyActivity extends AppCompatActivity {

    private static final String EXTRA_USER_ID = "user_id";
    private static final String EXTRA_CHAT_ID = "chat_id";

    private static final String STATE_SCROLL_POSITION = "scroll_position";

    @NonNull
    public static Intent makeIntent(@NonNull final Context context,
                                    @NonNull final String userId,
                                    @NonNull final String chatId) {
        return new Intent(context, MyActivity.class)
            .putExtra(EXTRA_USER_ID, userId)
            .putExtra(EXTRA_CHAT_ID, chatId);
    }

    @Override
    protected void onCreate(@Nullable final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        final Intent incomingIntent = getIntent();
        final String userId = incomingIntent.getStringExtra(EXTRA_USER_ID);
        final String chatId = incomingIntent.getStringExtra(EXTRA_CHAT_ID);

        final int scrollPosition;
        if (savedInstanceState == null) {
            scrollPosition = 0;
        } else {
            scrollPosition = savedInstanceState.getInt(STATE_SCROLL_POSITION);
        }

        // Do other stuff
    }


    @Override
    protected void onSaveInstanceState(@NonNull final Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(STATE_SCROLL_POSITION, ...);
    }

}
```


### Basic `Fragment` conventions

Always create a `newInstance` method for any `Fragment`, use `ARG_` prefix for the constants with input params and `STATE_` prefix for state saving:

```java
public final class MyFragment extends Fragment {

    private static final String ARG_USER_ID = "user_id";
    private static final String ARG_CHAT_ID = "chat_id";

    private static final String STATE_SCROLL_POSITION = "scroll_position";

    @NonNull
    public static MyFragment newInstance(@NonNull final String userId
                                         @NonNull final String chatId) {
        final MyFragment fragment = new MyFragment();
        final Bundle args = new Bundle(2);
        args.putString(ARG_USER_ID, userId);
        args.putString(ARG_CHAT_ID, chatId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onViewCreated(@NonNull final View view, @Nullable final Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // Also can be done in the onCreate method
        final Bundle args = getArguments();
        if (args == null) {
            return;
        }
        final String userId = args.getString(ARG_USER_ID);
        final String chatId = args.getString(ARG_CHAT_ID);

        final int scrollPosition;
        if (savedInstanceState == null) {
            scrollPosition = 0;
        } else {
            scrollPosition = savedInstanceState.getInt(STATE_SCROLL_POSITION);
        }

        // Do other stuff
    }


    @Override
    public void onSaveInstanceState(@NonNull final Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(STATE_SCROLL_POSITION, ...);
    }

}
```


### Method declaration wrapping

If a method declaration is too long for a single line, wrap lines like this:

```java
@NonNull
public String giveMeSomeString(@NonNull final String name,
                               @MyCell.ViewType final int viewType,
                               @Nullable final View containerView,
                               @NonNull final Object lock) throws WillNotException {
    // Smart code here
}
```

### Method invocation wrapping

If a method invocation is too long for a single line, wrap lines like this:

```java
final String smartResult = doSomethingWithAllThis(
    myName,
    viewType,
    containerView,
    smartLock
);
```


### As immutable as possible

It's just a general rule of thumb: 
> Always make your code as immutable as possible


### As method-local as possible

It's another rule of thumb: 
> Always make your state (and your variables) as local (method-local) as possible


### (A la) sealed classes factory methods naming conventions

Factory methods in your (a la) [sealed classes](005-recycler-view.md#sealed-cell-class) should be named with a `new` prefix:

```java
public final class ResultCell {

    // Some stuff here
    
    @NonNull
    public static ResultCell newSearchResult(@NonNull final SearchResult searchResult) {
        return new ResultCell(
                ViewType.SEARCH_RESULT,
                searchResult,
                null,
                0,
                null,
                false
        );
    }

    @NonNull
    public static ResultCell newShowMoreButton(@NonNull final String buttonText, @ColorInt final int buttonColor) {
        return new ResultCell(
                ViewType.SHOW_MORE_BUTTON,
                null,
                buttonText,
                buttonColor,
                null,
                false
        );
    }

    @NonNull
    public static ResultCell newTitle(@NonNull final String titleText, final boolean bold) {
        return new ResultCell(
                ViewType.TITLE,
                null,
                null,
                0,
                titleText,
                bold
        );
    }

    // And more stuff here

}
```


### `AitaJson` and `AitaJsonArray` variables naming conventions

`AitaJson` and `AitaJsonArray` variables should be named with `Json` and `JsonArray` postfixes respectively:

#### ✅ Good:

```java
final AitaJson json = ...;
if (json == null) {
    return null;
}
final AitaJson nameJson = json.optJson("name");
if (nameJson == null) {
    return null;
}
final AitaJsonArray partsJsonArray = nameJson.optJsonArray("parts");
if (partsJsonArray == null) {
    return null;
}
```


#### ❌ Bad:

```java
final AitaJson data = ...;
if (data == null) {
    return null;
}
final AitaJson name = data.optJson("name");
if (name == null) {
    return null;
}
final AitaJsonArray parts = name.optJsonArray("parts");
if (parts == null) {
    return null;
}
```


### `try-with-resources`

When you work with anything `AutoCloseable` or `Closeable` (e.g. `InputStream` or `Cursor`) use `try-with-resources` instead of invoking `.close` manually in the `finally` block:

```java
try (final Cursor cursor = readableDatabase.rawQuery(...)) {
    // ...
} catch (final SomeException e) {
    // ...
}
```


### Handling clicks

There are some issues with click handling:
1. It's possible to accidentally click some `View` multiple times which can lead to some strange behavior
1. Developers usually forget to send an analytics event on click event

In order to fix that we **must** use `AitaClickListener` with any `View.setOnClickListener` invocation. Note that you should create a new instance of `AitaClickListener` for each `View`. It can be used in several ways:

#### Simple click handling, do not send any events
```java
photoImageView.setOnClickListener(new AitaClickListener(
    AitaClickListener.EXPLICIT_NO_EVENT,
    v -> doSmth()
));
```

#### Simple click handling, send some event when click happens
```java
photoImageView.setOnClickListener(new AitaClickListener(
    "myFeature_myScreen_clickHappened",
    v -> doSmth()
));
```

#### Handle click after a small delay (for animation or smth)
```java
photoImageView.setOnClickListener(new AitaClickListener(
    "myFeature_myScreen_deferredClickHappened",
    true,
    v -> doSmth()
));
```

## Checkstyle rules


## Annotations

### AnnotationLocation
Annotations should be on their own line:
#### ✅ Good:
```java
@Nullable 
private String name;

@NonNull 
public static String giveMeSomething() {
    // ...
}
```
#### ❌ Bad:
```java
@Nullable private String name;

@NonNull public static String giveMeSomething() {
    // ...
}
```


### AnnotationUseStyle
#### ✅ Good:
```java
@Override
@SuppressWarnings("unchecked")
@IntDef({
    Test.ONE,
    Test.TWO,
})
```
#### ❌ Bad:
```java
@Override()
@SuppressWarnings({"unchecked"})
@IntDef({Test.ONE, Test.TWO})
```


## Block Checks

### AvoidNestedBlocks
Avoid unnecessary nested blocks. Nested blocks inside `switch` blocks are OK.
#### ✅ Good:
```java
private void foo() {
    switch (str) {
        case "hello": {
            final int x = 5;
            // ...
            break;
        }
        default:
            // ...
    }
}
```
#### ❌ Bad:
```java
private void foo() {
    {
        // This block is not needed
    }
    switch (str) {
        case "hello": {
            final int x = 5;
            // ...
        } // This is bad
            break;
        default:
            // ...
    }
}
```


### EmptyBlock
Checks for empty blocks:
#### ✅ Good:
```java
if (something) {
    statementShouldBeHere();
}
```
#### ❌ Bad:
```java
if (something) {
}
```


### EmptyCatchBlock
Empty `catch` block is a bad thing. Suppress it with a comment or with a varible name.
#### ✅ Good:
```java
try {
    doSomething();
} catch (final SomeException e) {
    // Expected
}
try {
    doSomething();
} catch (final SomeException expected) {}
```
#### ❌ Bad:
```java
try {
    doSomething();
} catch (final SomeException e) {
}
try {
    doSomething();
} catch (final SomeException e) {}
```


### LeftCurly
Checks for the placement of `{`.
#### ✅ Good:
```java
private void foo() {
    // ...
}
```
#### ❌ Bad:
```java
private void foo() 
{
    // ...
}
```


### NeedBraces
Explicit `{}` braces are required everywhere.
#### ✅ Good:
```java
if (something) {
    doSomething();
}
for (int i = 0; i < size; i++) {
    doSomething();
}
while (true) {
    doSomething();
}
```
#### ❌ Bad:
```java
if (something) doSomething();
if (something) 
    doSomething();
for (int i = 0; i < size; i++);
while (true)
    doSomething();
```


### RightCurlySame
`}` brace placement on the same line rule.
#### ✅ Good:
```java
try {
    doA();
} catch (final SomeException e) {
    doB();
} finally {
    doC();
}

if (something) {
    doA();
} else if (something) {
    doB();
} else {
    doC();
}

do {
    doA();
} while (something);
```
#### ❌ Bad:
```java
try {
    doA();
}
catch (final SomeException e) {
    doB();
}
finally {
    doC();
}

if (something) {
    doA();
}
else if (something) {
    doB();
}
else {
    doC();
}

do {
    doA();
}
while (something);
```


### RightCurlyAloneOrSingleLine
`}` brace placement on the next line rule.
#### ✅ Good:
```java
public final class Test {

    class Foo {
        private final String str;
    } 
    
    private void foo() {
        for (int i = 0; i < size; i++) {
            doA();
        } 
        while (true) {
            doB();
        }
    } 
    
    static {
        doC();
    } 
    
    {
        doD();
    }

}
```
#### ❌ Bad:
```java
public final class Test {

    class Foo {
        private final String str;
    } private void foo() {
        for (int i = 0; i < size; i++) {
            doA();
        } while (true) {
            doB();
        }
    } static {
        doC();
    } {
        doD();
    }

}
```


## Class Design

### FinalClass
Checks that a class which has only private constructors is declared as final.
#### ✅ Good:
```java
public final class Test {

    private Test() {}

}
```
#### ❌ Bad:
```java
public class Test {

    private Test() {}

}
```


### HideUtilityClassConstructor
Makes sure that utility classes (classes that contain only static methods or fields in their API) do not have a public constructor.
#### ✅ Good:
```java
public final class Test {

    public static void foo() {
        // ...
    }

    private Test() {}

}
```
#### ❌ Bad:
```java
public final class Test {

    public static void foo() {
        // ...
    }

}
```


### InnerTypeLast
Check nested (inner) classes/interfaces are declared at the bottom of the class after all method and field declarations.
#### ✅ Good:
```java
public final class Test {

    public static void doSomething() {
        // ...
    }

    private Test() {}

    public interface TestListener {
        void onTest();
    }

}
```
#### ❌ Bad:
```java
public final class Test {

    public interface TestListener {
        void onTest();
    }

    public static void doSomething() {
        // ...
    }

    private Test() {}

}
```


### InterfaceIsType
It is inappropriate to define an interface that does not contain any methods but only constants or is a marker-interface.
#### ✅ Good:
```java
public interface TestListener {
    void shouldHave();
}

public interface TestInterface {
    void someMethods();
}
```
#### ❌ Bad:
```java
public interface TestListener {
    String TEST = "test";
}

public interface TestInterface {}
```


### MutableException
Ensures that exception classes are immutable.
#### ✅ Good:
```java
public final class MyException extends RuntimeException {
    private final String info;

    // ...
}
```
#### ❌ Bad:
```java
public final class MyException extends RuntimeException {
    private String info;

    // ...
}
```


### OneTopLevelClass
Checks that each top-level class, interface or enum resides in a source file of its own.
#### ✅ Good:
```java
/* Begin file Test.java */
package tools;

public final class Test {
    private Test() {}
}
/* End file Test.java */

/* Begin file Hello.java */
package tools;

class Hello {
    void foo();
}
/* End file Hello.java */
```
#### ❌ Bad:
```java
/* Begin file Test.java */
package tools;

public final class Test {
    private Test() {}
}

class Hello {
    void foo();
}
/* End file Test.java */
```


### ThrowsCount
Restricts throws statements in a declaration count to **3**.
#### ✅ Good:
```java
private void foo() throws NullPointerException, FileNotFoundException, IOException {
    // ...
}
```
#### ❌ Bad:
```java
private void foo() throws NullPointerException, FileNotFoundException, IOException, IllegalFormatException {
    // ...
}
```


### VisibilityModifier
Only static final class members may be public; other class members must be private.
#### ✅ Good:
```java
public static final String NAME = "name";
public static final String HOME = "home";

private final String lastName = "lastName";
```
#### ❌ Bad:
```java
public final String name = "name";
protected final String home = "home";
final String lastName = "lastName";
```


## Coding

### ArrayTrailingComma
Checks that array initialization contains a trailing comma.
#### ✅ Good:
```java
private static final int[] ARR = new int[]{
    1,
    2,
    3,
};
```
#### ❌ Bad:
```java
private static final int[] ARR = new int[]{
    1,
    2,
    3
};
```


### CovariantEquals
Checks that classes which define a covariant `equals()` method also override method `equals(Object)`.
#### ✅ Good:
```java
public final class Test {

    public boolean equals(@Nullable final Test o) {
        // ...
    }

    @Override
    public boolean equals(@Nullable final Object o) {
        // ...
    }

    @Override
    public int hashCode() {
        // ...
    }

}
```
#### ❌ Bad:
```java
public final class Test {

    public boolean equals(@Nullable final Test o) {
        // ...
    }

}
```


### DeclarationOrder
The parts of a class or interface declaration should appear in the following order:
1. Class (static) variables. First the public class variables, then protected, then package level (no access modifier), and then private
1. Instance variables. First the public class variables, then protected, then package level (no access modifier), and then private
1. Methods
* Constructor can be anywhere

#### ✅ Good:
```java
public final class Test {

    public static final String WHAT = "what";

    private final String name;

    public Strig getName() {
        return name;
    }

    private void doSomething() {
        // ...
    }

}
```
#### ❌ Bad:
```java
public final class Test {

    private void doSomething() {
        // ...
    }

    public Strig getName() {
        return name;
    }

    private final String name;

    public static final String WHAT = "what";

}
```


### DefaultComesLast
Check that the `default` is after all the cases in a `switch` statement.
#### ✅ Good:
```java
switch (something) {
    case 1:
        doA();
        break;
    case 2:
        doB();
        break;
    default:
        doC();
}
```
#### ❌ Bad:
```java
switch (something) {
    default:
        doC();
        break;
    case 1:
        doA();
        break;
    case 2:
        doB();
        break;
}
```


### EmptyStatement
Detects empty statements (standalone `;` semicolon).
#### ✅ Good:
```java
if (someCondition) {
    doConditionalStuff();
}
doUnconditionalStuff();
```
#### ❌ Bad:
```java
if (someCondition);
    doConditionalStuff();
doUnconditionalStuff();
```


### EqualsAvoidNull
Checks that any combination of `String` literals is on the left side of an `equals()` comparison.
#### ✅ Good:
```java
final String myStr = null;
final boolean hello = "HELLO".equals(myStr);
if ("WORLD".equals(myStr)) {
    doA();
}
```
#### ❌ Bad:
```java
final String myStr = null;
final boolean hello = myStr != null && myStr.equals("HELLO");
if (myStr != null && myStr.equals("WORLD")) {
    doA();
}
```


### EqualsHashCode
Checks that classes that either override `equals()` or `hashCode()` also overrides the other.
#### ✅ Good:
```java
public final class Test {

    @Override
    public boolean equals(@Nullable final Object o) {
        // ...
    }

    @Override
    public int hashCode() {
        // ...
    }

}
```
#### ❌ Bad:
```java
public final class Test {

    @Override
    public boolean equals(@Nullable final Object o) {
        // ...
    }

}
```


### ExplicitInitialization
Checks if any class or object member is explicitly initialized to default for its type value (`null` for object references, `zero` for numeric types and `char` and `false` for `boolean`). 
#### ✅ Good:
```java
private int num;
private boolean flag;
private Object obj;
```
#### ❌ Bad:
```java
private int num = 0;
private boolean flag = false;
private Object obj = null;
```


### FallThrough
Checks for fall-through in `switch` statements. Finds locations where a case contains Java code but lacks a `break`, `return`, `throw` or `continue` statement. Fall-through comment is `// Fall through`.
#### ✅ Good:
```java
switch (something) {
    case 1:
        doA();
        // Fall through
    case 2:
        doB();
        // Fall through
    default:
        doC();
}
```
#### ❌ Bad:
```java
switch (something) {
    case 1:
        doA();
    case 2:
        doB();
    default:
        doC();
}
```


### FinalLocalVariable
Checks that local variables and parameters that never have their values changed are declared `final`.
#### ✅ Good:
```java
private void foo(final String str) {
    final int a = 1;
    // ...
}
```
#### ❌ Bad:
```java
private void foo(String str) {
    int a = 1;
    // ...
}
```


### IllegalThrows
This check is used to ensure that types are not declared to be thrown. Declaring that a method `throws java.lang.Error` or `java.lang.RuntimeException` is almost never acceptable. 
#### ✅ Good:
```java
private void foo() throws MyError {
    // ...
}

private void bar() throws SomeException {
    // ...
}
```
#### ❌ Bad:
```java
private void foo() throws Error {
    // ...
}

private void bar() throws RuntimeException {
    // ...
}
```


### IllegalToken
Labels are prohibited.
#### ❌ Bad:
```java
testlbl: for (int i = 0; i <= max; i++) {
    // ...
    while (something) {
        // ...
        continue testlbl;
    }
    break testlbl;
}
```


### IllegalType
Checks that particular classes or interfaces are never used.
#### ✅ Good:
```java
private final Map<String, String> map = new HashMap<>();
private final Set<String> set = new HashSet<>();
private final List<String> list = new ArrayList<>();
```
#### ❌ Bad:
```java
private final HashMap<String, String> map = new HashMap<>();
private final HashSet<String> set = new HashSet<>();
private final ArrayList<String> list = new ArrayList<>();
```


### InnerAssignment
Checks for assignments in subexpressions, such as in `String s = Integer.toString(i = 2);`
#### ✅ Good:
```java
final int num = -1;
final String str = String.valueOf(num);
```
#### ❌ Bad:
```java
final int num;
final String str = String.valueOf(num = -1);
```


### MagicNumber
Checks that there are no "magic numbers" where a magic number is a numeric literal that is not defined as a constant. `-1`, `0`, `1`, and `2` are not considered to be magic numbers.
#### ✅ Good:
```java
private static final int COUNT = 10;

private void foo() {
    for (int i = 0; i < COUNT; i++) {
        doA();
    }
}
```
#### ❌ Bad:
```java
private void foo() {
    for (int i = 0; i < 10; i++) {
        doA();
    }
}
```


### MissingSwitchDefault
Checks that `switch` statement has a `default` clause.
#### ✅ Good:
```java
switch (something) {
    case 1:
        doA();
        break;
    case 2:
        doB();
        break;
    default:
}
```
#### ❌ Bad:
```java
switch (something) {
    case 1:
        doA();
        break;
    case 2:
        doB();
        break;
}
```


### ModifiedControlVariable
Check for ensuring that for loop control variables are not modified inside the `for` block.
#### ✅ Good:
```java
for (int i = 0; i < size; i += 2) {
    // ...
}
```
#### ❌ Bad:
```java
for (int i = 0; i < size; i++) {
    i++;
    // ...
}
```


### MultipleStringLiterals
Checks for multiple occurrences of the same string literal within a single file (<=2 occurences are allowed).
#### ✅ Good:
```java
public final class Test {

    private static final String KEY_NAME = "name"
    private static final String COL_NAME = "name"

    private final String name;

    public Test(@NonNull final AitaJson json) {
        name = json.optString(KEY_NAME);
    }

    @NonNull
    public AitaJson toJson() {
        return new AitaJson().put(KEY_NAME, name);
    }

    @NonNull
    public ContentValues toContentValues() {
        final ContentValues cv = new ContentValues();
        cv.put(COL_NAME, name);
        return cv;
    }

}
```
#### ❌ Bad:
```java
public final class Test {

    private final String name;

    public Test(@NonNull final AitaJson json) {
        name = json.optString("name");
    }

    @NonNull
    public AitaJson toJson() {
        return new AitaJson().put("name", name);
    }

    @NonNull
    public ContentValues toContentValues() {
        final ContentValues cv = new ContentValues();
        cv.put("name", name);
        return cv;
    }

}
```


### MultipleVariableDeclarations
Checks that each variable declaration is in its own statement and on its own line.
#### ✅ Good:
```java
final String a = "a";
final String b = "b";
```
#### ❌ Bad:
```java
final String a = "a", b = "b";
```


### NestedForDepth
Restricts nested `for` blocks depth to `1`.
#### ✅ Good:
```java
for (int i = 0; i < size; i++) {
    for (int k = 0; k < length; k++) {
        acceptable();
    }
}
```
#### ❌ Bad:
```java
for (int i = 0; i < size; i++) {
    for (int k = 0; k < length; k++) {
        for (int m = 0; m < width; m++) {
            tooMuch();
        }
    }
}
```


### NestedIfDepth
Restricts nested `if-else` blocks depth to `1`.
#### ✅ Good:
```java
if (a) {
    if (b) {
        acceptable();
    }
}
```
#### ❌ Bad:
```java
if (a) {
    if (b) {
        if (c) {
            tooMuch();
        }
    }
}
```


### NestedTryDepth
Restricts nested `try` blocks depth to `1`.
#### ✅ Good:
```java
try {
    doA();
    try {
        doB();
    }
}
```
#### ❌ Bad:
```java
try {
    doA();
    try {
        doB();
        try {
            tooMuch();
        }
    }
}
```


### NoClone
Checks that the `clone` method is not overridden from the `Object` class.
#### ❌ Bad:
```java
public final class Test {

    @Override
    protected Object clone() throws CloneNotSupportedException {
        // ...
    }

}
```


### NoFinalizer
Verifies there are no `finalize()` methods defined in a class.
#### ❌ Bad:
```java
public final class Test {

    @Override
    protected void finalize() throws Throwable {
        super.finalize();
        // ...
    }

}
```


### OneStatementPerLine
Checks that there is only one statement per line.
#### ✅ Good:
```java
doA();
doB();
doC();
```
#### ❌ Bad:
```java
doA(); doB(); doC();
```


### OverloadMethodsDeclarationOrder
Checks that overload methods are grouped together.
#### ✅ Good:
```java
public void foo(final int i) {}

public void foo(final String s) {}

public void foo(final int i, final String s) {}

public void notFoo() {}
```
#### ❌ Bad:
```java
public void foo(final int i) {}

public void foo(final String s) {}

public void notFoo() {}

public void foo(final int i, final String s) {}
```


### PackageDeclaration
Ensures that a class has a package declaration, and whether the package name matches the directory name for the source file.
#### ✅ Good:
```java
/* Begin file tools/Test.java */
package tools;

public final class Test {}
/* End file tools/Test.java */

/* Begin file tools/Foo.java */
package tools;

public final class Foo {}
/* End file tools/Foo.java */
```
#### ❌ Bad:
```java
/* Begin file tools/Test.java */
package bar;

public final class Test {}
/* End file tools/Test.java */

/* Begin file tools/Foo.java */
public final class Foo {}
/* End file tools/Foo.java */
```


### ParameterAssignment
Disallows assignment of parameters.
#### ✅ Good:
```java
private void foo(final String str) {
    final String newStr = str + "Hello";
}
```
#### ❌ Bad:
```java
private void foo(String str) {
    str += "Hello";
}
```


### RequireThis
Checks that references to instance variables and methods of the present object are explicitly of the form `this.varName` or `this.methodName(args)` if their names are overlapping with local names or parameters.
#### ✅ Good:
```java
public final class Test {

    private final String name;

    public Test(final String name) {
        this.name = name;
    }

}
```
#### ❌ Bad:
```java
public final class Test {

    private final String name;

    public Test(final String name) {
        name = name;
    }

}
```


### SimplifyBooleanExpression
Checks for over-complicated boolean expressions. Currently finds code like `if (b == true)`, `b || true`, `!false`, etc.
#### ✅ Good:
```java
if (a) {
    // ...
}
```
#### ❌ Bad:
```java
if (a == true) {
    // ...
}
```


### SimplifyBooleanReturn
Checks for over-complicated `boolean` return statements.
#### ✅ Good:
```java
return !valid();
```
#### ❌ Bad:
```java
if (valid()) {
    return false;
} else {
    return true;
}
```


### StringLiteralEquality
Checks that `String` literals are not used with `==` or `!=`.
#### ✅ Good:
```java
if ("something".equals(x)) {
    // ...
}
```
#### ❌ Bad:
```java
if (x == "something") {
    // ...
}
```


### UnnecessaryParentheses
Checks if unnecessary parentheses are used in a statement or expression.
#### ✅ Good:
```java
return x;
return x + 1;
int x = y / 2 + 1;
for (int i = 0; i < 10; i++) { ... }
t -= z + 1;
```
#### ❌ Bad:
```java
return (x);
return (x + 1);
int x = (y / 2 + 1);
for (int i = (0); i < 10; i++) { ... }
t -= (z + 1);
```


### UnnecessarySemicolonAfterTypeMemberDeclaration
Checks if unnecessary semicolon is used after type member declaration.
#### ✅ Good:
```java
public final class Test {

    int field = 10;

    {
        // ...
    }

    static {
        // ...
    }

    Test() {
        // ...
    }

    void method() {
        // ...
    }

}
```
#### ❌ Bad:
```java
public final class Test {
    ;
    {};
    static {};
    Test(){};
    void method() {};
    int field = 10;;
}
```


### UnnecessarySemicolonInEnumeration
Checks if unnecessary semicolon is in `enum` definitions. Semicolon is not needed if `enum` body contains only `enum` constants.
#### ✅ Good:
```java
enum One {
    A,
    B
}
```
#### ❌ Bad:
```java
enum One {
    A,
    B;
}
```


### UnnecessarySemicolonInTryWithResources
Checks if unnecessary semicolon is used in last resource declaration.
#### ✅ Good:
```java
try (Reader r4 = new PipedReader();
     Reader r5 = new PipedReader()) {
    doA();
}
```
#### ❌ Bad:
```java
try (Reader r4 = new PipedReader();
     Reader r5 = new PipedReader();) {
    doA();
}
```


## Imports

### AvoidStarImport
Checks that there are no import statements that use the `*` notation.
#### ✅ Good:
```java
import java.util.ArrayList;
import java.util.HashMap;
```
#### ❌ Bad:
```java
import java.util.*;
```


### AvoidStaticImport
Checks that there are no static import statements.
#### ✅ Good:
```java
ErrorLogger.logException(e);
```
#### ❌ Bad:
```java
import static com.aita.ErrorLogger.logException;

// ... and later
logException(e);
```


### RedundantImport
Checks for redundant import statements. An import statement is considered redundant if:
* It is a duplicate of another import. This is, when a class is imported more than once.
* The class non-statically imported is from the java.lang package, e.g. importing java.lang.String.
* The class non-statically imported is from the same package as the current package.

#### ❌ Bad:
```java
import java.lang.String;
```


### UnusedImports
Checks for unused import statements.


## Javadoc Comments

### InvalidJavadocPosition
Checks that Javadocs are located at the correct position.
#### ❌ Bad:
```java
@SuppressWarnings("serial")
/**
 * This comment looks like javadoc but it at an invalid location.
 * Therefore, the text will not get into TestClass.html and the check will produce a violation.
 */
public class TestClass {
}
```


### JavadocBlockTagLocation
Checks that a javadoc block tag appears only at the beginning of a line, ignoring leading asterisks and white space.
#### ❌ Bad:
```java
/**
 * Escaped tag &#64;version (OK)
 * Plain text with {@code @see} (OK)
 * A @custom tag (OK)
 * 
 * email@author (OK)
 * (@param in parentheses) (OK)
 * '@param in single quotes' (OK)
 * @since 1.0 (OK)
 * text @return (violation)
 * * @param (violation)
+* @serial (violation)
 * @see first (OK) @see second (violation)
 */
public int field;
```


## Miscellaneous

### ArrayTypeStyle
Checks the style of array type definitions.
#### ✅ Good:
```java
private final String[] arr;

private char[] getChars() {
    // ...
}
```
#### ❌ Bad:
```java
private final String arr[];

private char getChars()[] {
    // ...
}
```


### CommentsIndentation
Controls the indentation between comments and surrounding code. Comments are indented at the same level as the surrounding code.
#### ✅ Good:
```java
/* 
 * Good one
 */
private final double pi = 3.14d;

public void foo2() {
    foo3();
    // Good one
}
```
#### ❌ Bad:
```java
 /* 
  * Bad one
  */
private final double pi = 3.14d;

public void foo2() {
    foo3();
        // Bad one
}
```


### FinalParameters
Check that parameters for methods, constructors, catch and for-each blocks are final.
#### ✅ Good:
```java
public void foo2(final String str) {
    try {
        doA();
        for (final String line : lines) {
            doB(line);
        }
    } catch (final MyException e) {
        doC(e);
    }
}
```
#### ❌ Bad:
```java
public void foo2(String str) {
    try {
        doA();
        for (String line : lines) {
            doB(line);
        }
    } catch (MyException e) {
        doC(e);
    }
}
```


### Indentation
Checks correct indentation of Java code.
#### ✅ Good:
```java
public final class Test {

    public void foo2(final String str,
                     final String time) 
        throws NullPointerException {

        switch (something) {
            case 1:
                break;
            default:
        }

        final int[] arr = new int[]{
            1,
            2,
            3,
        };
    }

}
```
#### ❌ Bad:
```java
public final class Test {

  public void foo2(final String str,
    final String time) 
  throws NullPointerException {

        switch (something) {
        case 1:
            break;
        default:
        }

        final int[] arr = new int[]{
          1,
          2,
          3,
        };
  }

}
```


### OuterTypeFilename
Checks that the outer type name and the file name match. For example, the class `Foo` must be in a file named `Foo.java`.


### TodoComment
A check for `TODO` comments. It won't pass until there are no such comments in the code. More details on such comments are [here](#todo-comments). Checked formats are:
```java
// TODO: REMOVE
// TODO: TASK
// TODO: TRANSLATE
// TODO: UNCOMMENT
```


### UncommentedMain
Checks for uncommented `main()` methods. Just because we can.


### UpperEll
Checks that long constants are defined with an upper ell. That is `L` and not `l`.
#### ✅ Good:
```java
final long num = 0L;
```
#### ❌ Bad:
```java
final long num = 0l;
```


## Modifiers

### ModifierOrder
Checks that the order of modifiers conforms to the suggestions in the Java Language specification:
1. `public`
1. `protected`
1. `private`
1. `abstract`
1. `default`
1. `static`
1. `final`
1. `transient`
1. `volatile`
1. `synchronized`
1. `native`
1. `strictfp`
#### ✅ Good:
```java
public abstract static class AbsFoo {
    // ...
}
```
#### ❌ Bad:
```java
static abstract public class AbsFoo {
    // ...
}
```


### RedundantModifier
Checks for redundant modifiers in:
1. `interface` and annotation definitions.
1. `final` modifier on methods of `final` and anonymous classes.
1. Inner `interface` declarations that are declared as `static`.
1. Class constructors.
1. Nested `enum` definitions that are declared as `static`.
#### ✅ Good:
```java
public interface Listener {
    void onEvent(String event);
}
```
#### ❌ Bad:
```java
public static interface Listener {
    public void onEvent(final String event);
}
```


## Naming conventions

### AbbreviationAsWordInName
Validates abbreviations (consecutive capital letters) length in identifier name.
#### ✅ Good:
```java
public final class AitaFragment {
    private final String userId;
    private final String photoUrl;
}
```
#### ❌ Bad:
```java
public final class AITAFragment {
    private final String userID;
    private final String photoURL;
}
```


### AbstractClassName
`abstract` classes should be named with an `Abs` prefix.
#### ✅ Good:
```java
public abstract class AbsAitaFragment {
    // ...
}

public final class AitaActivity {
    // ...
}
```
#### ❌ Bad:
```java
public abstract class AitaFragment {
    // ...
}

public final class AbsAitaActivity {
    // ...
}
```


### ClassTypeParameterName
Checks that class type parameter names consist of a single uppercase letter.
#### ✅ Good:
```java
public final class MyClass1<T> {}

public final class MyClass2<T> {}
```
#### ❌ Bad:
```java
public final class MyClass1<template> {}

public final class MyClass2<Template> {}
```


### ConstantName
Checks that constant names conform to a format: `^[A-Z][A-Z0-9]*(_[A-Z0-9]+)*$`. A constant is a `static` and `final` field or an interface/annotation field, except `serialVersionUID` and `serialPersistentFields`.
#### ✅ Good:
```java
public static final String NAME_KEY = "name";
```
#### ❌ Bad:
```java
public static final String nameKey = "name";
```


### InterfaceTypeParameterName
Checks that interface type parameter names consist of a single uppercase letter.
#### ✅ Good:
```java
public interface Foo<T> {
    // ...
}

public interface Bar<T> {
    // ...
}
```
#### ❌ Bad:
```java
public interface Foo<Template> {
    // ...
}

public interface Bar<template> {
    // ...
}
```


### LambdaParameterName
Check to verify lambda parameter names: `^[a-z][a-zA-Z0-9]*$`.
#### ✅ Good:
```java
final Listener listener = (final String name, final String address) -> {
    // ...
}
```
#### ❌ Bad:
```java
final Listener listener = (final String _ignore, final String Address) -> {
    // ...
}
```


### LocalFinalVariableName
Checks that local `final` variable names conform to a format: `^[a-z][a-zA-Z0-9]*$`.
#### ✅ Good:
```java
final String num = 15;
try (final InputStream stream = getSome(num)) {
    doA(stream);
} catch (final IOException error) {
    doE(error);
}
```
#### ❌ Bad:
```java
final String NUM = 15;
try (final InputStream STREAM = getSome(NUM)) {
    doA(STREAM);
} catch (final IOException ERROR) {
    doE(ERROR);
}
```


### LocalVariableName
Checks that local non-`final` variable names conform to a format: `^[a-z][a-zA-Z0-9]*$`.
#### ✅ Good:
```java
String sum = 0;
```
#### ❌ Bad:
```java
String SUM = 0;
```


### MemberName
Checks that instance variable names conform to a format: `^[a-z]{2}[a-zA-Z0-9]*$`.
#### ✅ Good:
```java
private Context context;
```
#### ❌ Bad:
```java
private Context mContext;
```


### MethodName
Checks that method names conform to a format: `^[a-z](_?[a-zA-Z0-9]+)*$`.
#### ✅ Good:
```java
public final class Foo {

    private String giveMeSome() {
        // ...
    }

    @Test
    public void buttonClicked_screenChangedToAdd() {
        // ...
    }

}
```
#### ❌ Bad:
```java
public final class Foo {

    public void Foo() {
        // ...
    }

    private String GiveMeSome() {
        // ...
    }

}
```


### MethodTypeParameterName
Checks that method type parameter names conform to a format: `^[A-Z]$`.
#### ✅ Good:
```java
private <T> String giveMeSome() {
    // ...
}

private <T> void gotcha(final String some) {
    // ...
}
```
#### ❌ Bad:
```java
private <Template> String giveMeSome() {
    // ...
}

private <template> void gotcha(final String some) {
    // ...
}
```


### PackageName
Checks that package names conform to a format: `^[a-z]+(\.[a-z][a-z0-9]*)*$`.
#### ✅ Good:
```java
package some.package.here;

package some.pack;
```
#### ❌ Bad:
```java
package somePackageHere;

package some_pack;
```


### ParameterName
Checks that method parameter names conform to a format: `^[a-z][a-zA-Z0-9]*$`.
#### ✅ Good:
```java
private void gotcha(final String name) {
    // ...
}
```
#### ❌ Bad:
```java
private void gotcha(final String NAME) {
    // ...
}
```


### StaticVariableName
Checks that `static`, non-`final` variable names conform to a format: `^[A-Z][A-Z0-9]*(_[A-Z0-9]+)*$`.
#### ✅ Good:
```java
public static String REQUEST_PREFIX = "...";
```
#### ❌ Bad:
```java
public static String requestPrefix = "...";
```


### TypeName
Checks that type names for classes, interfaces, enums, and annotations conform to a format: `^[A-Z][a-zA-Z0-9]*$`.
#### ✅ Good:
```java
public class Go {}

public interface myListener {}
```
#### ❌ Bad:
```java
public class GO {}

public interface my_listener {}
```


## Size violations

### OuterTypeNumber
Checks for the number of types declared at the outer (or root) level in a file. Only 1 type is permitted.
#### ✅ Good:
```java
/* Begin file Test.java */
public final class Test {
    // ...
}
/* End file Test.java */

/* Begin file Listener.java */
interface Listener {
    // ...
}
/* End file Listener.java */
```
#### ❌ Bad:
```java
/* Begin file Test.java */
public final class Test {
    // ...
}

interface Listener {
    // ...
}
/* End file Test.java */
```


### LineLength
Checks for long lines (140 symbols is max).
#### ✅ Good:
```java
@Nullable
@Override
public View onCreateView(@NonNull final LayoutInflater inflater,
                         @Nullable final ViewGroup container,
                         @Nullable final Bundle savedInstanceState) {
    // ...
}
```
#### ❌ Bad:
```java
@Nullable
@Override
public View onCreateView(@NonNull final LayoutInflater inflater, @Nullable final ViewGroup container, @Nullable final Bundle savedInstanceState) {
    // ...
}
```


## Whitespace

### EmptyForInitializerPad
Whitespace before the `for` initializer is forbidden.
#### ✅ Good:
```java
for (; i < j; i++, j--) {
    // ...
}
```
#### ❌ Bad:
```java
for ( ; i < j; i++, j--) {
    // ...
}
```


### EmptyForIteratorPad
Whitespace after the `for` iterator is forbidden.
#### ✅ Good:
```java
for (final Iterator foo = line.iterator(); foo.hasNext();) {
    // ...
}
```
#### ❌ Bad:
```java
for (final Iterator foo = line.iterator(); foo.hasNext(); ) {
    // ...
}
```


### EmptyLineSeparator
Checks for empty line separators after header, package, all import declarations, fields, constructors, methods, nested classes, static initializers and instance initializers.
#### ✅ Good:
```java
public final class Test {

    private final String name;
    private final String address;

    private void foo() {}

    private void bar() {}

    private void buzz() {}

}
```
#### ❌ Bad:
```java
public final class Test {

    private final String name;

    private final String address;


    private void foo() {}
    private void bar() {}


    private void buzz() {}

}
```


### GenericWhitespace
Checks that the whitespace around the Generic tokens (angle brackets) `<` and `>` are correct to the typical convention:

Left angle bracket (`<`):
* should be preceded with whitespace only in generic methods definitions.
* should not be preceded with whitespace when it is precede method name or following type name.
* should not be followed with whitespace in all cases.

Right angle bracket (`>`):
* should not be preceded with whitespace in all cases.
* should be followed with whitespace in almost all cases, except diamond operators and when preceding method name.
#### ✅ Good:
```java
public <K, V extends Number> void foo(K k, V v) {}

class Name<T1, T2> {}

private void foo() {
    OrderedPair<String, Box<Integer>> p;
    boolean same = Util.<Integer, String>compare(p1, p2);
    Pair<Integer, String> p1 = new Pair<>(1, "apple");
    List<T> list = ImmutableList.Builder<T>::new;
    sort(list, Comparable::<String>compareTo);
}
```
#### ❌ Bad:
```java
public <K, V extends Number>void foo(K k, V v) {}

class Name <T1, T2> {}

private void foo() {
    OrderedPair <String, Box<Integer>> p;
    boolean same = Util. <Integer, String> compare(p1, p2);
    Pair<Integer, String> p1 = new Pair <> (1, "apple");
    List<T> list = ImmutableList.Builder <T> ::new;
    sort(list, Comparable:: <String> compareTo);
}
```


### MethodParamPad
Checks the padding between the identifier of a method definition, constructor definition, method call, or constructor invocation; and the left parenthesis of the parameter list. There should be none.
#### ✅ Good:
```java
private void foo(final String name, final String address) {}

private void bar(final String name, final String address) {}
```
#### ❌ Bad:
```java
private void foo (final String name, final String address) {}

private void bar 
    (final String name, final String address) {}
```


### NoLineWrap
Checks that `package`, `import` and `import static` statements are not line-wrapped.
#### ✅ Good:
```java
package tools;

import java.util.ArrayList;

import static java.lang.String;
```
#### ❌ Bad:
```java
package 
    tools;

import 
    java.util.ArrayList;

import static 
    java.lang.String;
```


### NoWhitespaceAfterAndAllowLineBreaks
Whitespace after array initializer (`{`) is forbidden, but line break is allowed.
#### ✅ Good:
```java
private final int[] arr = new int[]{1, 2, 3};
// OR
private final int[] arr = new int[]{
    1,
    2,
    3,
};
```
#### ❌ Bad:
```java
private final int[] arr = new int[]{ 1, 2, 3};
```


### NoWhitespaceAfterAndNoLineBreaks
No whitespace and line breaks are allowed after `@`, `++x`, `--x`, `+x`, `-x`, `~x`, `!x`, `.do()`, `[` and `::method`.
#### ✅ Good:
```java
@ViewType
final int x;

--x;
++x;

-x;
+x;

~x;
!x;

final int[] arr = new int[1];

arr[0];

this::foo;
```
#### ❌ Bad:
```java
@ ViewType
final int x;

-- x;
++ x;

- x;
+ x;

~ x;
! x;

final int [] arr = new int [1];

arr [0];

this:: foo;
```


### NoWhitespaceBefore
Checks that there is no whitespace before `,`, `;`, `x++`, `x--`, `...` and `this::`.
#### ✅ Good:
```java
private void foo(final String... args) {
    go(a, b);

    x++;
    x--;

    this::foo;
}
```
#### ❌ Bad:
```java
private void foo(final String ... args) {
    go(a , b) ;

    x ++;
    x --;

    this ::foo;
}
```


### OperatorWrap
If you need to wrap a line, start a new line with an operator.
#### ✅ Good:
```java
final int size = list == null
    ? 0
    : list.size();
final int width = paddingLeft
    + contentWidth
    + paddingRight;
```
#### ❌ Bad:
```java
final int size = list == null ? 
    0 : 
    list.size();
final int width = paddingLeft + 
    contentWidth + 
    paddingRight;
```


### ParenPad
Spaces are forbidden after a left parenthesis and before a right parenthesis.
#### ✅ Good:
```java
doStuff(name, address);
```
#### ❌ Bad:
```java
doStuff( name, address );
```


### SeparatorWrapNewLine
`.` and `::` should be wrapped with a new line.
#### ✅ Good:
```java
obj
    .doStuff();
this
    ::foo;
```
#### ❌ Bad:
```java
obj.
    doStuff();
this::
    foo;
```


### SeparatorWrapEndOfLine
`,`, `;`, `...`, `(`, `[]{` and `]` should be wrapped with end of line.
#### ✅ Good:
```java
private void foo(final String... 
    args) {
    doStuff(
        a, 
        b
    );
    final int[] arr = new int[]{
        1,
        2,
        3,
    };
    arr[0];
}
```
#### ❌ Bad:
```java
private void foo(final String
    ... args) {
    doStuff
    (
        a
        , b
    )
    ;
    final int[] arr = new int[]{1,
        2,
        3,
    };
    arr[0
    ];
}
```


### SingleSpaceSeparator
Checks that non-whitespace characters are separated by no more than one whitespace.
#### ✅ Good:
```java
doStuff(a, b);
```
#### ❌ Bad:
```java
doStuff(a,  b);
```


### TypecastParenPad
In a typecast space is forbidden after a left parenthesis and before a right parenthesis.
#### ✅ Good:
```java
final TextView tv = (TextView) findViewById();
```
#### ❌ Bad:
```java
final TextView tv = ( TextView ) findViewById();
```


### WhitespaceAfter
`,`, `;`, `(Type)`, `if`, `else`, `while`, `do` and `for` should have a space after them (when needed).
#### ✅ Good:
```java
doStuff(a, b); goGetMe();
final TextView tv = (TextView) findViewById();
if (true) {
    // ...
} else {
    // ...
}
while (true) {
    // ...
}
do {
    // ...
} while (true);
```
#### ❌ Bad:
```java
doStuff(a,b);goGetMe();
final TextView tv = (TextView)findViewById();
if(true) {
    // ...
}else{
    // ...
}
while(true) {
    // ...
}
do{
    // ...
}while(true);
```


### WhitespaceAround
Checks that a token is surrounded by whitespace.
#### ✅ Good:
```java
final int x = 5;
switch (x) {
    // ...
}
try {
    // ...
} catch (final MyException expected) {}
```
#### ❌ Bad:
```java
final int x=5;
switch(x){
    // ...
}
try{
    // ...
}catch(final MyException expected) {}
```


### FileTabCharacter
Checks that there are no tab characters (`'\t'`) in the source code.
