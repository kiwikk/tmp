[<- Contents](README.md)

# ViewModel and LiveData

## Contents
1. [Create or get `ViewModel`](#create-or-get-viewmodel)
1. [Reset `ViewModel`](#reset-viewmodel)
1. [`ViewModel.onCleared`](#viewmodeloncleared)
1. [Return `LiveData` from `ViewModel`](#return-livedata-from-viewmodel)
1. [`LiveData` - `set` and `post`](#livedata---set-and-post)
1. [`SingleEventLiveData`](#singleeventlivedata)
1. [`MediatorLiveData`](#mediatorlivedata)
1. [Observing `LiveData` in `Activity`](#observing-livedata-in-activity)
1. [Note on `Fragment` `View`s leaks](#note-on-fragment-views-leaks)
1. [Observing `LiveData` in `Fragment`](#observing-livedata-in-fragment)
1. [Updating `RecyclerView` with `LiveData`](#updating-recyclerview-with-livedata)

## Create or get `ViewModel`
To retrieve `ViewModel` instance use `new ViewModelProvider(acitivity).get(MyViewModel.class)` in `Activity.onCreate` or `new ViewModelProvider(fragment).get(MyViewModel.class)` in `Fragment.onCreate` or `Fragment.onViewCreated`. Note that your `ViewModel` should never hold any kind of reference to your `Activity` or `Fragment`.

## Reset `ViewModel`
We often need to pass some kind of input to our `ViewModel`s or to access it from another `Fragment`. In this case use `Activity` as host of your `ViewModel` and do not use `ViewModel`'s constructor or `ViewModelProvider.Factory`. Instead of them use the following scheme:
1. Create the static inner `Input` class in your `ViewModel`:
    ```java
    public final class MyViewModel extends ViewModel {

        // ...
        
        public static final class Input {
            final String tripId;
            final String desiredFlightId;

            public Input(@NonNull final String tripId, @Nullable final String desiredFlightId) {
                this.tripId = tripId;
                this.desiredFlightId = desiredFlightId;
            }

            @Override
            @SuppressWarnings({"SimplifiableIfStatement", "RedundantIfStatement", "ConstantConditions"})
            public boolean equals(final Object o) {
                if (this == o) {
                    return true;
                }
                if (o == null || getClass() != o.getClass()) {
                    return false;
                }

                final Input input = (Input) o;

                if (tripId != null ? !tripId.equals(input.tripId) : input.tripId != null) {
                    return false;
                }
                if (desiredFlightId != null ? !desiredFlightId.equals(input.desiredFlightId) : input.desiredFlightId != null) {
                    return false;
                }

                return true;
            }

            @SuppressWarnings("ConstantConditions")
            @Override
            public int hashCode() {
                int result = tripId != null ? tripId.hashCode() : 0;
                result = 31 * result + (desiredFlightId != null ? desiredFlightId.hashCode() : 0);
                return result;
            }
        }

    }
    ```
1. Implement the `reset` method like that:
    ```java
    public final class MyViewModel extends ViewModel {

        // ...
        
        @Nullable
        private Input input;
        
        public void reset(@NonNull final Input input) {
            if (input.equals(this.input)) {
                return;
            }
            this.input = input;
            
            stateLiveData.setValue(null);
            // TODO: reset all the other stuff here
        }

    }
    ```
1. After that just use this method whenever you need to initialize your `ViewModel` in your `onCreate` or `onViewCreated` after you retrieved it's instance. This hack allows us to prevent resetting the `ViewModel` with the same data, to access it without counting on the `ViewModelProvider.Factory` undocumented behavior, and not to pass all the required arguments between our `Fragment`s.
    ```java
    final MyViewModel myViewModel = new ViewModelProvider(this).get(MyViewModel.class);
    myViewModel.reset(new Input(tripId, null));
    ```

## `ViewModel.onCleared`
Place your `ViewModel` cleanup in this method. Cancel requests and tasks, stop timers, etc.
```java
public final class MyViewModel extends ViewModel {

    // ...

    @Override
    protected void onCleared() {
        super.onCleared();
        volley.cancelAll(RQ_TAG);
        timeoutHandler.removeCallbacks(timeoutRunnable);
    }

}
```

## Return `LiveData` from `ViewModel`
Never expose your `LiveData` as `MutableLiveData` or `SingleEventLiveData`. Expose it as `LiveData` and create `ViewModel` methods like `onDateSelected` to pass data back from some `Fragment`s or `DialogFragment`s.
```java
public final class MyViewModel extends ViewModel {

    // ...

    private final MutableLiveData<MyState> stateLiveData = new MutableLiveData<>();
    private final SingleEventLiveData<String> errorLiveData = new SingleEventLiveData<>();
    private final SingleEventLiveData<MyNavigation> navigationLiveData = new SingleEventLiveData<>();

    @NonNull
    public LiveData<MyState> getState() {
        return stateLiveData;
    }

    @NonNull
    public LiveData<String> getError() {
        return errorLiveData;
    }

    @NonNull
    public LiveData<MyNavigation> getNavigation() {
        return navigationLiveData;
    }

}
```

## `LiveData` - `set` and `post`
There are usually no need to use `LiveData.postValue` method. Sometimes we use it with sockets, for example, but usually you recieve your values from tasks or requests on the main thread. Therefore `LiveData.setValue` method should be used in most cases.

## `SingleEventLiveData`
`SingleEventLiveData` is a special `MutableLiveData` subclass that delivers data to exactly one observer exactly once:
```java
/**
 * A lifecycle-aware observable that sends only new updates after subscription, used for events like
 * navigation and Snackbar messages.
 * <p>
 * This avoids a common problem with events: on configuration change (like rotation) an update
 * can be emitted if the observer is active. This LiveData only calls the observable if there's an
 * explicit call to setValue() or call().
 * <p>
 * Note that only one observer is going to be notified of changes.
 */
public class SingleEventLiveData<T> extends MutableLiveData<T> {

    private static final String TAG = "SingleEventLiveData";

    private final AtomicBoolean mPending = new AtomicBoolean(false);

    @MainThread
    @Override
    public void observe(@NonNull final LifecycleOwner owner, @NonNull final Observer<T> observer) {
        if (hasActiveObservers()) {
            MainHelper.log(TAG, "Multiple observers registered but only one will be notified of changes.");
        }

        // Observe the internal MutableLiveData
        super.observe(owner, new Observer<T>() {
            @Override
            public void onChanged(@Nullable final T t) {
                if (mPending.compareAndSet(true, false)) {
                    observer.onChanged(t);
                }
            }
        });
    }

    @MainThread
    @Override
    public void setValue(@Nullable final T t) {
        mPending.set(true);
        super.setValue(t);
    }

    /**
     * Used for cases where T is Void, to make calls cleaner.
     */
    @MainThread
    public void call() {
        setValue(null);
    }

    /**
     * Used for cases where T is Void from bg thread, to make calls cleaner.
     */
    @MainThread
    public void postCall() {
        postValue(null);
    }

}
```

Use it when you need to display an error or navigate to another `Activity`/`Fragment`/`DialogFragment`/etc.
```java
public final class MyViewModel extends ViewModel {

    // ...

    private final SingleEventLiveData<String> errorLiveData = new SingleEventLiveData<>();
    private final SingleEventLiveData<MyNavigation> navigationLiveData = new SingleEventLiveData<>();

    @NonNull
    public LiveData<String> getError() {
        return errorLiveData;
    }

    @NonNull
    public LiveData<MyNavigation> getNavigation() {
        return navigationLiveData;
    }

}

public final class MyFragment extends Fragment {

    @Override
    public void onViewCreated(@NonNull final View view, @Nullable final Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        final MyViewModel myViewModel = new ViewModelProvider(this).get(MyViewModel.class);
        myViewModel.getError().observe(getViewLifecycleOwner(), new Observer<String>() {
            @Override
            public void onChanged(@Nullable final String error) {
                if (MainHelper.isDummyOrNull(error)) {
                    return;
                }
                SnackbarUtil.make(view, error, Snackbar.LENGTH_LONG).show();
            }
        });
        myViewModel.getNavigation().observe(getViewLifecycleOwner(), new Observer<MyNavigation>() {
            @Override
            public void onChanged(@Nullable final MyNavigation navigation) {
                @MyNavigation.Type final int type = navigation.getType();
                switch (type) {
                    case MyNavigation.Type.PROFILE:
                        showFragment(...);
                        break;
                    case MyNavigation.Type.SETTINGS:
                        showFragment(...);
                        break;
                    default:
                        throw new IllegalArgumentException("Unknown Type: " + type);
                }
            }
        });
    }

}
```

**Important note 1:** using `Snackbar` to display errors is nice, but if use this technique with `Fragment` always make sure that the root `View` of your `Fragment` is either `FrameLayout` (recommended) or `CoordinatorLayout` (rare cases).

**Important note 2:** always use `getViewLifecycleOwner` as `LifecycleOwner` for `LiveData.observe` when working with `Fragment`. Otherwise, you will suffer from hard-to-find bugs. But more on that later.

## `MediatorLiveData`
Just don't use it. It's nearly useless. It will not process your data if it's not observed at the moment, which limits it's usages. Always remember, it's not a familiar `Observable`, it's `LiveData` :)

## Observing `LiveData` in `Activity`
Observing `LiveData` from `Activity` is simple:
```java
public static final class MyActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        final RecyclerView recyclerView = findViewById(...);
        final MyViewModel myViewModel = new ViewModelProvider(this).get(MyViewModel.class);
        myViewModel.getState().observe(this, new Observer<MyState>() {
            @Override
            public void onChanged(@Nullable final MyState state) {
                if (state == null) {
                    return;
                }
                // Update your views here
            }
        });
    }

}
```

## Note on `Fragment` `View`s leaks
You should always limit the scope of `View` references as much as possible. This rule applies to `Activity` as well, but let's see, why is it important to `Fragment`.

If you write the code like this:
```java
public final class MyFragment extends Fragment {
    
    private RecyclerView recyclerView;

    @Override
    public void onViewCreated(@NonNull final View view, @Nullable final Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        recyclerView = view.findViewById(...);
    }

}
```

and then your `Fragment` is replaced with another one (`FragmentTransaction.replace`) and put into backstack, then your `Fragment` and reference to your `View` are still alive, but the `View` is already destroyed. When you return to your `Fragment`, a new `View` will be created, but the old `View` is unreachable for garbage collection until then. It's easy to solve this issue by limiting the scope of the `View` to the `onViewCreated` method: 
```java
public final class MyFragment extends Fragment {

    @Override
    public void onViewCreated(@NonNull final View view, @Nullable final Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        final RecyclerView recyclerView = view.findViewById(...);
    }

}
```

However, sometimes we have to access the `View` in several methods of a `Fragment`. In this case just set your `View` references to `null` in `onDestroyView`:
```java
public final class MyFragment extends Fragment {

    @Nullable
    private RecyclerView recyclerView;

    @Override
    public void onViewCreated(@NonNull final View view, @Nullable final Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        recyclerView = view.findViewById(...);
        // ...
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        recyclerView = null;
    }

}
```

## Observing `LiveData` in `Fragment`
`Fragment`s have 2 different lifecycles: a lifecycle of a `Fragment` itself and a lifecycle of it's `View`. Which means that using `Fragment.this` with the `LiveData.observe` method will prevent your `Observer`s from removal on `Fragment`'s `View` destruction. Use `Fragment.getViewLifecycleOwner` method to prevent such behavior. It returns a `LifecycleOwner` that dispatches `create` and `destroy` events according to `Fragment`'s `View`.

Observing `LiveData` from `Fragment` is not much harder, than doing so from `Activity`:
```java
public final class MyFragment extends Fragment {
    
    private MyViewModel myViewModel;

    @Override
    public void onCreate(@Nullable final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        myViewModel = new ViewModelProvider(this).get(MyViewModel.class);
    }

    @Override
    public void onViewCreated(@NonNull final View view, @Nullable final Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        final RecyclerView recyclerView = view.findViewById(...);
        myViewModel.getState().observe(getViewLifecycleOwner(), new Observer<MyState>() {
            @Override
            public void onChanged(@Nullable final MyState state) {
                if (state == null) {
                    return;
                }
                // Update views accordingly
            }
        });
    }

}
```

**Important note:** you should always make your `View` logic as stateless, as possible. It means that you should always store everything in your `State` [sealed classes](005-recycler-view.md#sealed-cell-class) and react to events accordingly:
```java
public final class MyFragment extends Fragment {

    @Override
    public void onViewCreated(@NonNull final View view, @Nullable final Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        myViewModel.getState().observe(getViewLifecycleOwner(), new Observer<MyState>() {
            @Override
            public void onChanged(@Nullable final MyState state) {
                if (state == null) {
                    return;
                }
                @MyState.Type final int type = state.getType();
                switch (type) {
                    case MyState.Type.PROGRESS:
                        // Show progress, hide other stuff
                        break;
                    case MyState.Type.LIST:
                        // Show RecyclerView, hide everything else and update cells
                        break;
                    case MyState.Type.MAP:
                        // Show map, hide everything else and update map state
                        break;
                    default:
                        throw new IllegalArgumentException("Unknown Type: " + type);
                }
            }
        });
    }

}
```

## Updating `RecyclerView` with `LiveData`
To allow `RecyclerView` to restore it's state correctly when working with `LiveData`, always use the following approach:
```java
public final class MyFragment extends Fragment {

    @Override
    public void onViewCreated(@NonNull final View view, @Nullable final Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        final Context context = requireContext();
        final RecyclerView recyclerView = view.findViewById(...);
        recyclerView.setLayoutManager(new LinearLayoutManager(context, RecyclerView.VERTICAL, false));
        myViewModel.getCells().observe(getViewLifecycleOwner(), new Observer<List<MyCell>>() {
            @Override
            public void onChanged(@Nullable final List<MyCell> cells) {
                if (cells == null) {
                    return;
                }
                final RecyclerView.Adapter adapter = recyclerView.getAdapter();
                if (adapter == null) {
                    final MyAdapter myAdapter = new MyAdapter(...);
                    recyclerView.setAdapter(myAdapter);
                } else {
                    final MyAdapter myAdapter = (MyAdapter) adapter;
                    adapter.submitList(...);
                }
            }
        });
    }

}
```
