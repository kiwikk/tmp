[<- Contents](README.md)

# RecyclerView
In a few years we have developed a number of practices to work with `RecyclerView`, `RecyclerView.Adapter`, `RecyclerView.ViewHolder` and `DiffUtil`. These practices (or patterns) are described in this document and should be used for all the related components.

## Contents
1. [Action listeners](#action-listeners)
1. [Dependencies](#dependencies)
1. [`ViewHolder`](#viewholder)
1. [Simple cell class](#simple-cell-class)
1. [`Adapter` with single view type](#adapter-with-single-view-type)
1. [Sealed cell class](#sealed-cell-class)
1. [`Adapter` with several view types](#adapter-with-several-view-types)
1. [`DiffUtil`](#diffutil)
1. [Templates](#templates)

## Action listeners
There is always a need to react to certain events that occur in a `ViewHolder`'s `View`. Such events should never be handled inside `Adapter` or `ViewHolder`. You should extract a corresponding `interface` inside your `Adapter`, pass an instance of the `interface` through the `Adapter` constructor and then pass it to your `ViewHolder`s' constructors (more on that in [ViewHolder](#viewholder)). That `interface` should be declared like that:
```java
public final class TripListAdapter extends RecyclerView.Adapter<TripListHolder> {

    private final TripListActionListener tripListActionListener;

    public MyAdapter(..., @NonNull final TripListActionListener tripListActionListener) {
        ...
        this.tripListActionListener = tripListActionListener;
    }

    public interface TripListActionListener {
        void onShowPastClick();
        void onTripClick(int adapterPosition);
        void onTripLongClick(int adapterPosition);
    }

}
```

And that `Adapter` should be created like that:
```java
... = new MyAdapter(..., new TripListActionListener() { // Often is implemented by ViewModel
    @Override
    public void onShowPastClick() {
        // ...
    }

    @Override
    public void onTripClick(final int adapterPosition) {
        // ...
    }

    @Override
    public void onTripLongClick(final int adapterPosition) {
        // ...
    }
});
```

## Dependencies
Besides action listeners `Adapter` usually needs `LayoutInflater`, `RequestManager`, `RecycledViewPool` for nested `RecyclerView`s, etc. This stuff should be provided through the `Adapter` constructor and passed to the `ViewHolder` constructor like this:
```java
public final class TripListAdapter extends RecyclerView.Adapter<TripListHolder> {

    private final LayoutInflater inflater;
    private final RequestManager requestManager;
    private final RecyclerView.RecycledViewPool stepsRecycledViewPool;
    private final TripListActionListener tripListActionListener;

    ...

    public TripListAdapter(@NonNull final LayoutInflater inflater,
                           @NonNull final RequestManager requestManager,
                           @NonNull final RecyclerView.RecycledViewPool stepsRecycledViewPool,
                           @NonNull final TripListActionListener tripListActionListener,
                           ...) {
        this.inflater = inflater;
        this.requestManager = requestManager;
        this.stepsRecycledViewPool = stepsRecycledViewPool;
        this.tripListActionListener = tripListActionListener;
        ...
    }

    @NonNull
    @Override
    public TripListHolder onCreateViewHolder(@NonNull final ViewGroup parent, @TripCell.ViewType final int viewType) {
        switch (viewType) {
            case ...:
                return new TripHolder(inflater, parent, requestManager, stepsRecycledViewPool, tripListActionListener);
            ...
        }
    }

    ...

}
```

## `ViewHolder`
`View` logic should be never placed inside the `onCreateViewHolder` or the `onBindViewHolder`. All the stuff related to `View`s should be placed into the `ViewHolder` descendant:
```java
public final class TripHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
    
    private final RequestManager requestManager;
    private final TripListActionListener tripListActionListener;
    
    private final TextView textView;
    private final Button button;

    public TripHolder(@NonNull final LayoutInflater inflater,
                      @NonNull final ViewGroup parent,
                      @NonNull final RequestManager requestManager,
                      @NonNull final TripListActionListener tripListActionListener) {
        super(inflater.inflate(R.layout..., parent, false));
        this.requestManager = requestManager;
        this.tripListActionListener = tripListActionListener;
        textView = itemView.findViewById(R.id...);
        button = itemView.findViewById(R.id...);
        button.setOnClickListener(this);
    }
    
    public void bind(@NonNull final TripCell cell) {
        textView.setText(cell.getTitle());
        button.setText(cell.getButtonText());
    }

    @Override
    public void onClick(final View v) {
        final int adapterPosition = getAdapterPosition();
        if (adapterPosition == RecyclerView.NO_POSITION) {
            return;
        }
        tripListActionListener.onTripClick(adapterPosition);
    }
    
}
```
Note that you should inflate views inside the `ViewHolder` constructor, not inside the `onBindViewHolder` method. Issue with inflating inside the `onBindViewHolder` is that to find the layout file of the `View` you had to jump to another class. To prevent this, just pass the `LayoutInflater` and `ViewGroup` to the `ViewHolder`.

Note also how the click action is handled in the previous example. You should always subscribe on the `View` events in the `ViewHolder` constructor and check the adapter position for the `RecyclerView.NO_POSITION` value.

## Simple cell class
Simple cell class is basically a [data class](004-data-classes.md) that represents items of `RecyclerView`. If you need to display only one view type inside a `RecyclerView` you should create a simple data class with all necessary fields, a constructor and getters. Any data processing inside the `bind` method of the `ViewHolder` should be avoided. If you need to format a date or a string or to display some other preprocessed data you should preprocess it in advance and pass it to the cell class constructor:
```java
public final class CheckoutCell {

    private final String dateText;
    private final String priceText;
    private final String title;

    public CheckoutCell(@NonNull final String dateText,
                        @NonNull final String priceText,
                        @NonNull final String title) {
        this.dateText = dateText;
        this.priceText = priceText;
        this.title = title;
    }

    public String getDateText() {
        return dateText;
    }

    public String getPriceText() {
        return priceText;
    }

    public String getTitle() {
        return title;
    }

}
```

Then in your `ViewHolder`'s `bind` method you'll use something like this:
```java
public void bind(@NonNull final CheckoutCell cell) {
    dateTextView.setText(cell.getDateText());
    priceTextView.setText(cell.getPriceText());
    titleTextView.setText(cell.getTitle());
}
```

## `Adapter` with single view type
When you need a simple `Adapter` for a single view type it should be structured like this:
```java
public final class CheckoutAdapter extends RecyclerView.Adapter<CheckoutAdapter.ItemHolder> {

    private final LayoutInflater inflater;

    private List<CheckoutCell> cells;

    public CheckoutAdapter(@NonNull final LayoutInflater inflater, @NonNull final List<CheckoutCell> cells) {
        this.inflater = inflater;
        this.cells = cells;
    }

    public void update(@NonNull final List<CheckoutCell> newCells) {
        cells = newCells;
        // Note: it's better to use AbsDiffableListAdapter
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ItemHolder onCreateViewHolder(@NonNull final ViewGroup parent, final int viewType) {
        return new ItemHolder(inflater, parent);
    }

    @Override
    public void onBindViewHolder(@NonNull final ItemHolder itemHolder, final int position) {
        itemHolder.bind(cells.get(position));
    }

    @Override
    public int getItemCount() {
        return cells.size();
    }

    static final class ItemHolder extends RecyclerView.ViewHolder {
        private final TextView textView;

        ItemHolder(@NonNull final LayoutInflater inflater, @NonNull final ViewGroup parent) {
            super(inflater.inflate(R.layout., parent, false));
            textView = itemView.findViewById(R.id.);
        }

        void bind(@NonNull final CheckoutCell cell) {
            // TODO:
        }
    }

}
```

## Sealed cell class
If you need to display several different view types in a `RecyclerView` just use the [sealed classes](004-data-classes.md#sealed-classes-and-enums) pattern.

## `Adapter` with several view types
`Adapter` that works with several view types in this case looks like this (again, use our [Live Templates](011-downloads.md#intellij-idea-live-templates) to generate this code):
```java
public final class ResultsAdapter extends RecyclerView.Adapter<AbsResultHolder> {

    @NonNull
    private final AbsResultCell.ViewType[] viewTypeValues = AbsResultCell.ViewType.values();

    private final LayoutInflater inflater;

    private List<AbsResultCell> cells;

    public ResultsAdapter(@NonNull final LayoutInflater inflater, @NonNull final List<AbsResultCell> cells) {
        this.inflater = inflater;
        this.cells = cells;
    }

    public void update(@NonNull final List<AbsResultCell> newCells) {
        cells = newCells;
        // Note: it's better to use AbsDiffableListAdapter
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public AbsResultHolder onCreateViewHolder(@NonNull final ViewGroup parent, final int viewTypeOrdinal) {
        final AbsResultCell.ViewType viewType = viewTypeValues[viewTypeOrdinal];
        switch (viewType) {
            case AbsResultCell.ViewType.SEARCH_RESULT:
                return new SearchResultHolder(inflater, parent);
            case AbsResultCell.ViewType.SHOW_MORE_BUTTON:
                return new ShowMoreButtonHolder(inflater, parent);
            case AbsResultCell.ViewType.TITLE:
                return new TitleHolder(inflater, parent);
            default:
                throw new IllegalArgumentException("Unknown ViewType: " + viewType);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull final AbsResultHolder holder, final int position) {
        holder.bind(cells.get(position));
    }

    @Override
    public int getItemCount() {
        return cells.size();
    }

    @Override
    public int getItemViewType(final int position) {
        return cells.get(position).getViewType().ordinal();
    }

}
```

`AbsResultHolder` here can also contain action listener, `RequestManager` and all the other necessary things and looks like this:
```java
public abstract class AbsResultHolder extends RecyclerView.ViewHolder {

    public AbsResultHolder(@NonNull final View itemView) {
        super(itemView);
    }

    public abstract void bind(@NonNull final AbsResultCell cell);

}
```

## `DiffUtil`
In all the previous examples we used `notifyDataSetChanged` to update data in a `RecyclerView`. `DiffUtil` allows us to automatically handle only necessary changes in our `List`s (like `notifyItemChanged`, `notifyItemInserted`, `notifyItemRemoved` and so on). Using `DiffUtil` is a preferred way of handling any top-level `RecyclerView` data changes. To use it we have to:
1. Implement `equals` and `hashCode` methods in our cell class
1. Make our cell class (or abstract base cell class) `OurClass` `implements Diffable<OurClass>`. In case of sealed class we need to add the `implements` part to the abstract base class and implement `isSame` and `isContentsSame` methods for each of our child cell classes.
1. Make our `Adapter` `extends AbsDiffableListAdapter<OurClass, OurViewHolder>`
1. Get rid of `update` and `getItemCount` methods in our `Adapter`
1. Remove any `List` references from `Adapter` and use the `getItem` method wherever needed
1. Use `AbsDiffableListAdapter.submitList(List<OurCellClass>)` method to update the `RecyclerView` contents

Our cell class starts looking like this:

```java
public abstract class AbsMyTripsCell implements Diffable<AbsMyTripsCell> {

    public enum ViewType {
        UNSORTED,
        TITLE,
        ARRIVE_AT,
        ...
    }

    @NonNull
    private final ViewType viewType;

    AbsMyTripsCell(@NonNull final ViewType viewType) {
        this.viewType = Throw.ifNull(viewType);
    }

    @NonNull
    public ViewType getViewType() {
        return viewType;
    }

    @Override
    @SuppressWarnings({"SimplifiableIfStatement", "RedundantIfStatement"})
    public boolean equals(final Object o) { ... }

    @Override
    public int hashCode() { ... }

    @Override
    @NonNull
    public String toString() { ... }

}
```

And child classes:

```java
public final class TitleMyTripsCell extends AbsMyTripsCell {

    @Nullable
    private final UnevenWeightTitle unevenWeightTitle;
    @Nullable
    private final String subtitle;
    @Nullable
    private final String timeText;

    public TitleMyTripsCell(@Nullable final UnevenWeightTitle unevenWeightTitle,
                            @Nullable final String subtitle,
                            @Nullable final String timeText) {
        super(ViewType.TITLE);
        this.unevenWeightTitle = unevenWeightTitle;
        this.subtitle = subtitle;
        this.timeText = timeText;
    }

    @Nullable
    public UnevenWeightTitle getUnevenWeightTitle() {
        return unevenWeightTitle;
    }

    @Nullable
    public String getSubtitle() {
        return subtitle;
    }

    @Nullable
    public String getTimeText() {
        return timeText;
    }

    @Override
    public boolean isSame(@NonNull final AbsMyTripsCell other) {
        if (this.getViewType() != other.getViewType()) {
            return false;
        }
        return this.equals(other);
    }

    @Override
    public boolean isContentsSame(@NonNull final AbsMyTripsCell other) {
        return this.equals(other);
    }

    @Override
    @SuppressWarnings({"SimplifiableIfStatement", "RedundantIfStatement"})
    public boolean equals(final Object o) { ... }

    @Override
    public int hashCode() { ... }

    @Override
    @NonNull
    public String toString() { ... }

}
```

And our `Adapter`:
```java
public final class MyTripsAdapter extends AbsDiffableListAdapter<AbsMyTripsCell, AbsMyTripsHolder> {

    @NonNull
    private final AbsMyTripsCell.ViewType[] viewTypeValues = AbsMyTripsCell.ViewType.values();

    @NonNull
    private final LayoutInflater inflater;
    @NonNull
    private final RequestManager requestManager;

    public MyTripsAdapter(@NonNull final List<AbsMyTripsCell> cells,
                          @NonNull final LayoutInflater inflater,
                          @NonNull final RequestManager requestManager) {
        super(cells);
        ...
    }

    @NonNull
    @Override
    public AbsMyTripsHolder onCreateViewHolder(@NonNull final ViewGroup parent, final int viewTypeOrdinal) {
        final AbsMyTripsCell.ViewType viewType = viewTypeValues[viewTypeOrdinal];
        switch (viewType) {
            case UNSORTED:
                return new UnsortedMyTripsHolder(inflater, parent, requestManager, listener);
            case TITLE:
                return new TitleMyTripsHolder(inflater, parent);
            case ARRIVE_AT:
                return new ArriveAtMyTripsHolder(inflater, parent, requestManager);
            // Other cases...
            default:
                throw new IllegalArgumentException("Unknown ViewType: " + viewType);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull final AbsMyTripsHolder holder, final int position) {
        holder.bind(getItem(position));
    }

    @Override
    public int getItemViewType(final int position) {
        return getItem(position).getViewType().ordinal();
    }

}
```

As always, you can use our [Live Templates](011-downloads.md#intellij-idea-live-templates) to generate this code.

`DiffUtil` also allows us to handle `ViewHolder` updates partially and we should have that on our minds. However right now that feature is not used in App in the Air Android app.

## Templates
You'll find a lot of useful Live Templates [here](011-downloads.md#intellij-idea-live-templates).
